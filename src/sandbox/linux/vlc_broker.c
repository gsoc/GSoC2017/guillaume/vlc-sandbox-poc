/******************************************************************************
* linux/vlc_broker.h: VLC broker - allows or denies access to system ressources
*******************************************************************************/

#include "vlc_broker.h"
#include "vlc_syscall_sets.h"
#include "vlc_sandbox_paths.h"
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*****************************************************************************
 * Static prototypes
 *****************************************************************************/
/* Delete mmap stack linked list */
static void delete_stack_nodes(vlc_stack_node_t *node_temp);
/* Find parent of a thread by matching stack addresses - recursive auxiliary function */
/* direction means up or down vlc_sandbox_node_t linked list */
static vlc_sandbox_node_t *search_matching_stack_rec(vlc_sandbox_node_t *node_temp, void *addr, int direction);
/* Handle clone syscall (threads are already allowed by seccomp filters, this function will take care of process creation) */
/* TODO: STILL EXPERIMENTAL! */
static void handle_allowed_clone(vlc_sandbox_node_t *node);
/* vlc_sigprocmask_node_t handling */
static vlc_sigprocmask_node_t *find_sigprocmask_node_by_tid(long tid);
static void delete_sigprocmask_node(vlc_sigprocmask_node_t *node);
static void append_sigprocmask_node(vlc_sigprocmask_node_t *node);
static void print_sigprocmask_nodes();

/****************************************************************************
* vlc_sigprocmask_node_t handling
*****************************************************************************/
static vlc_sigprocmask_node_t *find_sigprocmask_node_by_tid(long tid) {
    vlc_sigprocmask_node_t *node_temp = sandbox_sigprocmask_head_node;
    while(node_temp != NULL){
        if (node_temp->parent_tid == tid) {
            return node_temp;
        }
        node_temp = node_temp->next;
    }
    return NULL;
}

static void delete_sigprocmask_node(vlc_sigprocmask_node_t *node) {
    if (node != NULL) {
        /* If node == sandbox_sigprocmask_head_node, change head pointer and free structure */
        if (node->parent_tid == sandbox_sigprocmask_head_node->parent_tid) {
            if (node->next == NULL) {
                sandbox_sigprocmask_head_node = NULL;
            } else {
                sandbox_sigprocmask_head_node = node->next;
                sandbox_sigprocmask_head_node->prev = NULL;
            }
            free(node);
        } else {
            if (node->next == NULL) {
                node->prev->next = NULL;
            } else {
                node->next->prev = node->prev;
                node->prev->next = node->next;
            }
            free(node);
        }
    }
}

static void append_sigprocmask_node(vlc_sigprocmask_node_t *node) {
    if (sandbox_sigprocmask_head_node == NULL) {
        sandbox_sigprocmask_head_node = node;
    } else {
        vlc_sigprocmask_node_t *node_temp = sandbox_sigprocmask_head_node;
        while(node_temp->next != NULL) {
            node_temp = node_temp->next;
        }
        /* Append to last node */
        node_temp->next = node;
        node->prev = node_temp;
    }
}

void add_sigprocmask_node(long parent_tid) {
    /* Check if parent already made a sigprocmask call */
    vlc_sigprocmask_node_t *node = find_sigprocmask_node_by_tid(parent_tid);
    if (node == NULL) {
        /* create a new node and append if to the linked list */
        vlc_sigprocmask_node_t *new_node = malloc(sizeof(struct vlc_sigprocmask_node_t));
        new_node->parent_tid = parent_tid;
        new_node->next = NULL;
        new_node->prev = NULL;
        append_sigprocmask_node(new_node);
    } else {
        /* Remove node from list, and put it at the end */
        node->prev->next = node->next;
        node->next->prev = node->prev;
        append_sigprocmask_node(node);
    }
}

vlc_sigprocmask_node_t *pop_sigprocmask_node() {
    if (sandbox_sigprocmask_head_node != NULL) {
        vlc_sigprocmask_node_t *res;
        res = sandbox_sigprocmask_head_node;
        sandbox_sigprocmask_head_node = sandbox_sigprocmask_head_node->next;
        /* Sometimes syscalls order get messed up ... For instance: a newly created thread can call a set_robust_list before the clone syscall (that created this new thread) returns. The following code fixes this issue. */
        if (res->next != NULL) {
            return res->next;
        }
        return res;
    } else {
        return NULL;
    }
}

void delete_sigprocmask_node_from_tid(long parent_tid) {
    vlc_sigprocmask_node_t *node_temp = find_sigprocmask_node_by_tid(parent_tid);
    delete_sigprocmask_node(node_temp);
}

static void print_sigprocmask_nodes() {
    vlc_sigprocmask_node_t *current = sandbox_sigprocmask_head_node;
    int i = 0;
    printf("VLC Broker (PID: %ld): Printing sandbox_sigprocmask structure ...\n", get_current_pid());
    if (current != NULL) {
        printf("VLC Broker (PID: %ld): parent n°%d -> %ld\n", get_current_pid(), i, current->parent_tid);
        while (current->next != NULL) {
            i++;
            current = current->next;
            printf("VLC Broker (PID: %ld): parent n°%d -> %ld\n", get_current_pid(), i, current->parent_tid);
        }
        printf("VLC Broker (PID: %ld): Done!\n", get_current_pid());
    } else {
        printf("VLC Broker (PID: %ld): sandbox_sigprocmask_head_node empty!\n", get_current_pid());
    }
}

/****************************************************************************
* vlc_stack_node_t handling
*****************************************************************************/
/* Add new mmap entry to stack_node_head */
void add_stack_node(vlc_sandbox_node_t *node, void *addr, intptr_t stack_size) {
    if (node->vlc_sandbox_node_data->stack_node_head == NULL) {
        /* Init the first vlc_stack_node_t */
        vlc_stack_node_t *head = malloc(sizeof(struct vlc_stack_node_t));
        head->stack_starting_addr = addr;
        head->stack_size = stack_size;
        head->associated_tid = -1;
        head->next = NULL;
        node->vlc_sandbox_node_data->stack_node_head = head;
        return;
    } else {
        vlc_stack_node_t *node_temp;
        node_temp = node->vlc_sandbox_node_data->stack_node_head;
        /* The new vlc_stack_node_t will be appended at the end of the linked list */
        while(node_temp->next != NULL) {
            node_temp = node_temp->next;
        }
        vlc_stack_node_t *new_node = malloc(sizeof(struct vlc_stack_node_t));
        new_node->stack_starting_addr = addr;
        new_node->stack_size = stack_size;
        new_node->associated_tid = -1;
        new_node->next = NULL;
        node_temp->next = new_node;
        return;
    }
}

/* Set to -2 associated_tid in stack_node with address addr of parent thread */
/* This function is called when a child thread dies, and therefore doesn't use its stack anymore. A new child thread might use it. */
/* WARNING: madvise != munmap, we need to keep the stack addr but make it re-assignable by a new thread in our sandbox model. There won't be a new mmap syscall to book this memory, a new thread might do a set_robust_list on it without any mmap call from its parent! */
void free_stack_association(void *addr, long tid) {
    /* Find the parent */
    vlc_sem_wait(&sandbox_node_mutex);
    /* Find the sandbox node of the thread trying to madvise addr */
    vlc_sandbox_node_t *node = find_node_by_tid_rec(vlc_sandbox_node, tid);
    /* If we are lucky, the thread trying to madvise is freeing its own memory JUST BEFORE DYING. */
    int lucky = 0;
    if (node->parent != NULL) {
        /* A thread is freeing its own stack memory if we can find this stack in its parent's stack_nodes linked list */
        vlc_stack_node_t *stack_node_temp = node->parent->vlc_sandbox_node_data->stack_node_head;
        while(stack_node_temp != NULL) {
            if ((stack_node_temp->associated_tid == tid) && (stack_node_temp->stack_starting_addr == addr)) {
                /* Found! Set associated_tid to -2 means that a new child thread can now ask this addr in a set_robust_list */
                stack_node_temp->associated_tid = -2;
                lucky = 1;
            }
            stack_node_temp = stack_node_temp->next;
        }
    }
    /* If we are lucky, the thread is about to die, free its sandbox node IF it has no son threads (which means no stack_node_head) ... */
    if (lucky) {
        if (node->vlc_sandbox_node_data->stack_node_head == NULL) {
            delete_sandbox_node(node);
        }
    } else {
        /* If we are not lucky, then a thread if freeing memory for someone else, we need to find from which parent and set to -1 the associated_tid */
        int parent_found = 0;
        vlc_sandbox_node_t *temp_node = get_sandbox_node_head_rec(vlc_sandbox_node);
        while ((!parent_found) && (temp_node != NULL)) {
            vlc_stack_node_t *stack_node_temp = temp_node->vlc_sandbox_node_data->stack_node_head;
            while(stack_node_temp != NULL) {
                if ((stack_node_temp->associated_tid == tid) && (stack_node_temp->stack_starting_addr == addr)) {
                    /* It's a match! Set associated_tid to -2 means that a new thread can now ask this addr in a set_robust_list */
                    stack_node_temp->associated_tid = -2;
                    parent_found = 1;
                }
                stack_node_temp = stack_node_temp->next;
            }
            temp_node = temp_node->next;
        }
    }
    vlc_sem_post(&sandbox_node_mutex);
}

/* Delete mmap stack linked list */
static void delete_stack_nodes(vlc_stack_node_t *node_temp) {
    if (node_temp != NULL) {
        delete_stack_nodes(node_temp->next);
        free(node_temp);
    }
}

/*************************************************
 * Parent thread <-> son thread matching function
 *************************************************/
/* Find the parent thread of an unknown thread */
/* Returns NULL if not found */
vlc_sandbox_node_t *find_parent_node(vlc_sandbox_request_data_t *vlc_sandbox_request_data) {

    vlc_syscall_t *vlc_syscall = vlc_sandbox_request_data->vlc_syscall;

    /* Check syscall type (we might be able to guess parent another way in the future? ... SYS_set_robust_list might not be the only way to do this on Linux ...) */
    if (vlc_syscall->syscall == SYS_set_robust_list) {
        printf("VLC Broker (PID: %ld): set_robust_list from %ld\n", get_current_pid(), vlc_syscall->tid);
        vlc_sandbox_node_t *parent = search_matching_stack((void*) vlc_syscall->param1);
        if (parent == NULL) {
            /* Behind here means that the requested stack addr was previously used by another thread (and got reallocated) */
            /* Fall back to the sigprocmask strategy */
            print_sigprocmask_nodes();
            vlc_sigprocmask_node_t *sigprocmask_node = pop_sigprocmask_node();
            if (sigprocmask_node != NULL) {
                parent = find_node_by_tid(sigprocmask_node->parent_tid);
            } else {
                parent = NULL;
            }
        }
        return parent;
    } else {
        printf("VLC Broker (PID: %ld): Can't find a parent with syscall %d (TID: %ld)\n", get_current_pid(), vlc_syscall->syscall, vlc_syscall->tid);
        return NULL;
    }
}

/****************************************************************************
 * Parent thread <-> son thread matching functions (using STACK ADDRESSES)
 ***************************************************************************/
/* Find parent of a thread by matching stack addresses */
vlc_sandbox_node_t *search_matching_stack(void *addr) {
    vlc_sem_wait(&sandbox_node_mutex);
    printf("VLC Sandbox (PID: %ld): Looking for %p\n", get_current_pid(), addr);
    /* Look for a match in both direction (up and down the linked list) */
    vlc_sandbox_node_t *left;
    vlc_sandbox_node_t *right;
    if (vlc_sandbox_node->next != NULL) {
        right = vlc_sandbox_node->next;
    } else {
        right = NULL;
    }
    /* Parent is more likely to be on the left than on the right of the linked list (going left means towards higher TID) */
    left = search_matching_stack_rec(vlc_sandbox_node, addr, -1);
    if (left != NULL) {
        vlc_sem_post(&sandbox_node_mutex);
        return left;
    } else {
        right = search_matching_stack_rec(right, addr, 1);
        vlc_sem_post(&sandbox_node_mutex);
        return right;
    }
}

/* Find parent of a thread by matching stack addresses - recursive auxiliary function */
static vlc_sandbox_node_t *search_matching_stack_rec(vlc_sandbox_node_t *node_temp, void *addr, int direction) {
    if (node_temp != NULL) {
        /* Check if syscall address is in a child memory stack of node_temp */
        if (node_temp->vlc_sandbox_node_data->stack_node_head != NULL) {
            /* check all nodes of stack_node_head */
            vlc_stack_node_t *cursor;
            cursor = node_temp->vlc_sandbox_node_data->stack_node_head;
            while(cursor != NULL) {
                if ((addr > cursor->stack_starting_addr) && ((intptr_t) addr < ((intptr_t) cursor->stack_starting_addr + (intptr_t) cursor->stack_size)) && (cursor->associated_tid == -1)) {
                    /* It's a match! return parent */
                    printf("VLC Sandbox (PID: %ld): Parent found!\n", get_current_pid());
                    cursor->associated_tid = sandbox_request->tid;
                    return node_temp;
                }
                cursor = cursor->next;
            }
        }
        /* Nothing was found, go on */
        if (direction > 0) {
            return search_matching_stack_rec(node_temp->next, addr, direction);
        } else {
            return search_matching_stack_rec(node_temp->prev, addr, direction);
        }
    } else {
        return NULL;
    }
}

/*************************************
 * Filter out some calls (OS specific)
 *************************************/
/* Some syscalls need to be ignored (which means that the broker tells the handler that they were executed successfully eventhough the call didn't happen) */
int filter_out_request(vlc_sandbox_request_data_t *vlc_sandbox_request_data) {
    vlc_syscall_t *vlc_syscall = vlc_sandbox_request_data->vlc_syscall;

    /* Remove from sandbox_sigprocmask linked list the thread that requested vlc_syscall ... sigprocmask call and clone should happen one after the other, which means that either clone already happened, or the sigprocmask call wasn't preparing a clone. In both cases, we don't need to have an entry in sandbox_sigprocmask_node struct for the current thread (the thread that requested vlc_syscall) anymore. */
    delete_sigprocmask_node_from_tid(vlc_syscall->tid);

    /* SYS_rt_sigaction and SYS_rt_sigprocmask are to be handled carefully : we don't want to crash the thread but we don't want to let one set its own handler either */
	if ((vlc_syscall->syscall == SYS_rt_sigaction) || (vlc_syscall->syscall == SYS_rt_sigprocmask)) {
		/* setting access to 1 lets the thread go on without crashing but we don't apply the signal mask */
		sandbox_request->access = 1;
		sandbox_request->return_code = 0;
		printf("VLC Broker (PID: %ld): New signal handler blocked (TID: %ld) ...\n", get_current_pid(), vlc_syscall->tid);

        /* Save every call to rt_sigprocmask in a FIFO linked list. At the next set_robust_list call on a reallocated stack addr (in which case -> associated_tid = -2), his most probable parent will be the first element in list. */
        if (vlc_syscall->syscall == SYS_rt_sigprocmask) {
            add_sigprocmask_node(vlc_syscall->tid);
        }
        return 1;
    } else {
        return 0;
    }
}

/********************************
 * Syscall handling (OS specific)
 ********************************/
/* If this function is called, then the syscall in sandbox_request was approved */
void handle_allowed_syscall(vlc_sandbox_node_t *node) {

    vlc_syscall_t *vlc_syscall = sandbox_request->vlc_sandbox_request_data->vlc_syscall;

	sandbox_request->access = 1;
    /* Clone syscall has to be handled differently */
    if (vlc_syscall->syscall == SYS_clone) {
        handle_allowed_clone(node);
    } else {
        /* Make the syscall and write kernel's answer to sandbox_request structure */
    	sandbox_request->return_code = syscall(vlc_syscall->syscall, vlc_syscall->param1, vlc_syscall->param2, vlc_syscall->param3, vlc_syscall->param4, vlc_syscall->param5, vlc_syscall->param6);

    	/* Look for mmap with flag MAP_STACK (creating memory for son thread stack) */
    	if ((vlc_syscall->syscall == SYS_mmap) && ((vlc_syscall->param4 & MAP_STACK) == MAP_STACK) && (vlc_syscall->param3 == PROT_READ|PROT_WRITE) ) {
    		/* Return code is the address of the beginning of the son stack */
    		/* Second param is the stack size */
    		printf("VLC Broker (PID: %ld): MMAP WITH STACK (TID: %ld): starting at %p\n", get_current_pid(), vlc_syscall->tid, sandbox_request->return_code);
    		add_stack_node(node, (void*) sandbox_request->return_code, vlc_syscall->param2);
    	}

    	/* Look for madvise with flag MADV_DONTNEED (free memory of a thread stack before exit) */
    	if ((vlc_syscall->syscall == SYS_madvise) && ((vlc_syscall->param3 & MADV_DONTNEED) == MADV_DONTNEED)) {
    		/* This syscall freed the stack memory of a thread just before its death */
    		/* We need to catch it and reset to -1 the associated_tid in the vlc_mmap_stack_t node of the vlc_sandbox_node_t of the parent of this thread */
    		/* Indeed, a new child thread of this parent might re-use this memory space without calling for a new mmap with flag MAP_STACK */
    		printf("VLC Broker (PID: %ld): Freeing stack at %p (TID: %ld) ...\n", get_current_pid(), vlc_syscall->param1, vlc_syscall->tid);
    		free_stack_association((void*) vlc_syscall->param1, vlc_syscall->tid);
    	}
    }

    if (PRINT_SANDBOX_REQUESTS) {
        printf("VLC Broker (PID: %ld): return code %d\n", get_current_pid(), sandbox_request->return_code);
        if (sandbox_request->return_code == -1) {
            printf("    errno (PID: %ld): %d %s\n", get_current_pid(), errno, strerror(errno));
        }
        printf("VLC Broker (PID: %ld): Access granted!\n", get_current_pid());
    }
}

/* Handle clone syscall (threads are already allowed by seccomp filters, this function will take care of process creation) */
/* TODO: STILL EXPERIMENTAL! This is theorically possible but really tested yet ... */
static void handle_allowed_clone(vlc_sandbox_node_t *node) {
    /* The sandboxed thread is about to fork / vfork a new process. We need to create a new broker in a new process, then create a thread with the same sandbox rules as the sandboxed thread that tried to spawn it and finaly to make the new thread resume the execution of the sandboxed thread by jumping into its context ... good luck :D */
    /* vfork */
    vlc_syscall_t *vlc_syscall = sandbox_request->vlc_sandbox_request_data->vlc_syscall;

    if (EQUAL_FLAG(vlc_syscall->param1, CLONE_VM|CLONE_VFORK|SIGCHLD)) {
        printf("VLC Broker (PID: %ld): new VFORK !! ... spawning new broker!\n", get_current_pid());
        int pid;
        pid = fork();
        if (pid == 0) {
            printf("VLC Broker (PID: %ld): New process %d\n", get_current_pid(), get_current_pid());

            /* Save context and sandbox levels before cleaning up the parent's sandbox data structure. */
            ucontext_t ctx_sav;
            ctx_sav = *sandbox_request->vlc_sandbox_request_data->sandboxed_ctx;
            uint64_t flags = node->flags;
            int level = node->level;
            char **name = node->name;

            /* Clean up previous sandbox and create a new one */
            /* Delete vlc_sandbox_node data structure (we are in a new sandbox) */
            delete_sandbox_nodes();
            /* Reset sandbox_request */
            delete_sandbox_request();
            /* Create a new broker for this process and set the flags / level for the new process */
            vlc_duplicate_broker(flags, level, name);

            /* Overwrite the return register with the value 0 (indicating we are the new process) */
            ctx_sav.uc_mcontext.gregs[REG_RAX] = pid;
            printf("VLC Broker (PID: %ld): Jumping back into context of %d with return value %d ...\n", get_current_pid(), vlc_syscall->tid, ctx_sav.uc_mcontext.gregs[REG_RAX]);
            /* Jump back to the sandboxed thread context and hope for the best! */
            setcontext(&ctx_sav);

        } else {
            /* Parent process */
            int status;
            /* Block parent until child process is ready */
            if (waitpid(NULL, &status, 0) == -1) {
                perror("Waitpid() failed");
                exit(1);
            }
            if (WIFSIGNALED(status)) {
                printf("VLC Broker (PID: %ld): Child process terminated with error: %s\n", get_current_pid(), strsignal(WTERMSIG(status)));
            }
            printf("VLC Broker (PID: %ld): Child exit status: %d %s\n", get_current_pid(), status, strerror(status));
            printf("VLC Broker (PID: %ld): Parent is going on!! (son PID: %d)\n", get_current_pid(), pid);
            /* pid is the pid of the child process. Add security node in sandbox_node. */
            add_sandbox_node(pid, node->flags, node->level, node->name, node);
            /* set return_code */
            sandbox_request->return_code = pid;
            /* Parent process returns normally to handler */
        }
    } else if (EQUAL_FLAG(vlc_syscall->param1, CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD)) {
        printf("VLC Broker (PID: %ld): new FORK !! ... spawning new broker!\n", get_current_pid());
        int pid;
        pid = fork();
        if (pid == 0) {
            printf("VLC Broker (PID: %ld): New process %d\n", get_current_pid(), get_current_pid());

            /* Save context and sandbox levels before cleaning up the parent's sandbox data structure. */
            ucontext_t ctx_sav;
            ctx_sav = *sandbox_request->vlc_sandbox_request_data->sandboxed_ctx;
            uint64_t flags = node->flags;
            int level = node->level;
            char **name = node->name;

            /* Clean up previous sandbox and create a new one */
            /* Delete vlc_sandbox_node data structure (we are in a new sandbox) */
            delete_sandbox_nodes();
            /* Reset sandbox_request */
            delete_sandbox_request();
            /* Create a new broker for this process and set the flags / level for the new process */
            vlc_duplicate_broker(flags, level, name);

            /* Overwrite the return register with the value 0 (indicating we are the new process) */
            ctx_sav.uc_mcontext.gregs[REG_RAX] = pid;
            printf("VLC Broker (PID: %ld): Jumping back into context of %d with return value %d ...\n", get_current_pid(), vlc_syscall->tid, ctx_sav.uc_mcontext.gregs[REG_RAX]);
            /* Jump back to the sandboxed thread context and hope for the best! */
            setcontext(&ctx_sav);

        } else {
            /* Parent process */
            printf("VLC Broker (PID: %ld): Parent is going on!! (son PID: %d)\n", get_current_pid(), pid);
            /* pid is the pid of the child process. Add security node in sandbox_node. */
            add_sandbox_node(pid, node->flags, node->level, node->name, node);
            /* set return_code */
            sandbox_request->return_code = pid;
            /* Parent process returns normally to handler */
        }
    }
}


/*******************************************************************************
 * Allow or deny access to vlc_syscall following node security rules
 ******************************************************************************/
/* Check access to syscall for the thread with the sandbox flags and level in node */
/* Returns 1 if access granted */
int check_access(vlc_sandbox_node_t *node, vlc_sandbox_request_data_t *vlc_sandbox_request_data) {

    vlc_syscall_t *vlc_syscall = vlc_sandbox_request_data->vlc_syscall;

    if (PRINT_SANDBOX_REQUESTS) {
        printf("VLC Broker (PID: %ld): TID: %d (%s) UID %d: requests syscall %d\n", get_current_pid(), vlc_syscall->tid, node->name, vlc_syscall->uid, vlc_syscall->syscall);
    }

    if (HAS_FLAG(node->level, SANDBOX_DENY_ALL)) {
        printf("VLC Broker (PID: %ld): TID: %d (%s) requests syscall %d but has SANDBOX_DENY_ALL!\n", get_current_pid(), vlc_syscall->tid, node->name, vlc_syscall->syscall);
        return 0;
    } else {
        if (is_sandbox_required_syscall(vlc_syscall->syscall)) {
            /* Those syscalls must be allowed otherwise the sandbox won't work */
            return 1;
        } else if (is_syscall_with_path(vlc_syscall->syscall)) {
            int flag = SANDBOX_READ_REQUEST;
            /* For open syscall, check flags */
            if (vlc_syscall->syscall == SYS_open) {
                if (HAS_FLAG(vlc_syscall->param2, O_RDWR)) {
                    flag = SANDBOX_WRITE_REQUEST | SANDBOX_WRITE_REQUEST;
                } else if (HAS_FLAG(vlc_syscall->param2, O_WRONLY)) {
                    flag = SANDBOX_WRITE_REQUEST;
                }
            }
            return check_path_access((const char*) vlc_syscall->param1, vlc_syscall, node, flag);
        } else if (is_fd_creation_syscall(vlc_syscall->syscall)) {
            /* Check path permissions */
            return 1;
        } else if (is_fd_read_syscall(vlc_syscall->syscall)) {
            /* Get path from file descriptor */
            /* Check path permissions */
            return 1;
        } else if (is_fd_write_syscall(vlc_syscall->syscall)) {
            /* Get path from file descriptor */
            /* Check path permissions */
            return 1;
        } else if (is_admin_syscall(vlc_syscall->syscall)) {
            /* Those syscall should already be killed by seccomp filters ... */
            return 0;
        } else if (is_thread_or_process_creation_syscall(vlc_syscall->syscall)) {

            printf("VLC Broker (PID: %ld): Clone request from %ld with parameters:\n\tchild stack: %p\n\tflags: %d\n\ttype: ", get_current_pid(), vlc_syscall->tid, (void*) vlc_syscall->param2,(int) vlc_syscall->param1);

            if (EQUAL_FLAG(vlc_syscall->param1, CLONE_VM|CLONE_VFORK|SIGCHLD)) {
                /* vFork syscall */
                /* TODO: handle_allowed_clone is still in progress so for now, kill this thread. */
                printf("vFork\n");
                return 0;
            } else if (EQUAL_FLAG(vlc_syscall->param1, CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD)) {
                /* Fork syscall */
                /* TODO: handle_allowed_clone is still in progress so for now, kill this thread. */
                printf("Fork\n");
                return 0;
            } else {
                /* Unrecognised clone - kill by default (threads are already allowed by seccomp filters) */
                printf("unknown\n");
                return 0;
            }
        } else {
            /* By default deny access */
            printf("VLC Broker (PID: %ld): TID: %d (%s) UID %d: requests syscall %d\n", get_current_pid(), vlc_syscall->tid, vlc_syscall->uid, node->name, vlc_syscall->syscall);
            print_sandbox_nodes();
            printf("VLC Broker (PID: %ld): Unexpected syscall! Access denied!\n", get_current_pid());
            return 0;
        }
    }
}
