/*****************************************************************************
* linux/vlc_sandbox_internal.h: main functions of the thread sandbox on Linux
*****************************************************************************/

#include "../vlc_sandbox_internal.h"
#include "../vlc_sandbox_struct.h"

/*****************************************************************************
* vlc_stack_node_t handling
*****************************************************************************/
/* Add new entry to mmap_stack_head of node. This will be used to find parents of orphan threads */
void add_stack_node(vlc_sandbox_node_t *node, void *addr, intptr_t stack_size);
/* Reset to -1 associated_tid in stack_node with address addr of parent thread */
void free_stack_association(void *addr, long tid);

/****************************************************************************
* vlc_sigprocmask_node_t handling
*****************************************************************************/
void add_sigprocmask_node(long parent_tid);
vlc_sigprocmask_node_t *pop_sigprocmask_node();
void delete_sigprocmask_node_from_tid(long parent_tid);

/*****************************************************************************
* Parent thread - son thread matching functions
*****************************************************************************/
/* Find parent of a thread by matching stack addresses */
vlc_sandbox_node_t *search_matching_stack(void *addr);
