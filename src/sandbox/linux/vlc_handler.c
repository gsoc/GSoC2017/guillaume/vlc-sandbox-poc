/***********************************************************************************
 * vlc_handler.c: VLC handler - catch grey syscalls and request access to brocker
 ***********************************************************************************/

#include "../vlc_sandbox_internal.h"

#define LINUX_SIGSYS 31
#define SYS_SECCOMP 1

/* Registers Macros */
#define SYSCALL_REG(_ctx, _reg) ((_ctx)->uc_mcontext.gregs[(_reg)])
#define SYSCALL_RESULT(_ctx)    SYSCALL_REG(_ctx, REG_RAX)
#define SYSCALL_SYSCALL(_ctx)   SYSCALL_REG(_ctx, REG_RAX)
#define SYSCALL_IP(_ctx)        SYSCALL_REG(_ctx, REG_RIP)
#define SYSCALL_PARM1(_ctx)     SYSCALL_REG(_ctx, REG_RDI)
#define SYSCALL_PARM2(_ctx)     SYSCALL_REG(_ctx, REG_RSI)
#define SYSCALL_PARM3(_ctx)     SYSCALL_REG(_ctx, REG_RDX)
#define SYSCALL_PARM4(_ctx)     SYSCALL_REG(_ctx, REG_R10)
#define SYSCALL_PARM5(_ctx)     SYSCALL_REG(_ctx, REG_R8)
#define SYSCALL_PARM6(_ctx)     SYSCALL_REG(_ctx, REG_R9)

/*****************************************************************************
 * Handler function
 *****************************************************************************/
/* Catch SIGSYS signal and send sandbox_request to brocker */
void handler(int sig, siginfo_t *siginfo, void *context_ext) {

    /* Signal handlers should always preserve "errno" */
    const int old_errno = errno;
    /* Save the context of the sandboxed thread at the instruction that caused the SIGSYS */
    /* TODO: STILL IN PROGRESS: this context will be used by handle_allowed_clone to jump back to the sandboxed code */
    ucontext_t *context = (ucontext_t*) context_ext;

    /* Sanity checks to make sure that this SIGSYS comes from a BPF filter */
    if (sig != LINUX_SIGSYS || siginfo->si_code != SYS_SECCOMP || !context || siginfo->si_errno < 0) {
        printf("VLC Handler (PID: %ld): Unexpected SIGSYS received ...\n", get_current_pid());
        errno = old_errno;
        return;
    }

    /* Create syscall instance with parameters from context */
    vlc_syscall_t syscall_param;
    syscall_param.tid = get_current_tid();
    syscall_param.uid = (long) siginfo->si_uid;
    syscall_param.syscall = (long) SYSCALL_SYSCALL(context);
    syscall_param.param1 = SYSCALL_PARM1(context);
    syscall_param.param2 = SYSCALL_PARM2(context);
    syscall_param.param3 = SYSCALL_PARM3(context);
    syscall_param.param4 = SYSCALL_PARM4(context);
    syscall_param.param5 = SYSCALL_PARM5(context);
    syscall_param.param6 = SYSCALL_PARM6(context);

    /* Request access to sandbox_request */
    vlc_sem_wait(&sandbox_request->mutex);
    /* Write request parameters (syscall_param, context & tid) to sandbox_request */
    sandbox_request->vlc_sandbox_request_data->sandboxed_ctx = context;
    sandbox_request->vlc_sandbox_request_data->vlc_syscall = &syscall_param;
    sandbox_request->tid = syscall_param.tid;

    /* Wake up broker and wait for answer */
    vlc_sem_post(&sandbox_request->broker);
    vlc_sem_wait(&sandbox_request->handler);

    /* Check access answer */
    if (sandbox_request->access == 1) {
        /* Overwrite return register with broker's return code value */
        SYSCALL_RESULT(context) = sandbox_request->return_code;
        /* Release sandbox_request mutex  */
        vlc_sem_post(&sandbox_request->mutex);
        /* Restore errno */
        errno = old_errno;
        /* Let the thread go on and hope for the best! */
    } else {
        printf("VLC Handler (PID: %ld): Broker denied access, exiting ...\n", get_current_pid());
        /* Release sandbox_request mutex  */
        vlc_sem_post(&sandbox_request->mutex);
        /* Restore errno */
        errno = old_errno;
        /* Abort! A thread is trying to do something illegal! */
        abort();
    }
}
