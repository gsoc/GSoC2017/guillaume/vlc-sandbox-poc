/***********************************************************************************
 * vlc_handler.h: VLC handler - catch grey syscalls and request access to brocker
 ***********************************************************************************/

/* Catch SIGSYS signal and ask access to brocker */
void handler(int sig, siginfo_t *siginfo, void *context);
