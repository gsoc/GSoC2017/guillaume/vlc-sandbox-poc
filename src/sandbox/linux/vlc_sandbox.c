/*************************************************************
 * linux/vlc_sandbox.c: main functions of the sandbox on Linux
 *************************************************************/

#include "../vlc_sandbox_internal.h"

/******************************
 * Global variable declarations
 ******************************/
vlc_sigprocmask_node_t *sandbox_sigprocmask_head_node = NULL;

/*********************************
* Sandbox initialization functions
**********************************/

/* Initialize sigaction to catch SIGSYS signals. */
/* SIGSYS signals will be sent to a thread that called a grey syscall. Use our handler to take deal with it. */
static int init_sigaction() {
    /* Set up sigaction */
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    /* Sigaction handler */
    act.sa_sigaction = &handler;
    /* The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. */
    act.sa_flags = SA_SIGINFO;
    return (sigaction(SIGSYS, &act, NULL) < 0);
}

/* On linux, the sandbox uses sigaction to catch SIGSYS signal. */
/* See init_sigaction() for more details. */
int vlc_sandbox_init_spec(libvlc_int_t *p_libvlc) {

	if(init_sigaction() != 0) {
        printf("VLC Sandbox (PID: %ld): Sigaction wasn't set properly ...\n", get_current_pid());
        return 1;
    }
    /* The current sandbox needs to keep track of stack addresses creation very carefully, and avoid new signal handlers in order to work. This is the reason why we create a blacklist sandbox at the very beginning of libvlc initialization. */
	/* Start a seccomp filter in order to catch mmap with MAT_STACK flag and set_robust_list (for thread creation), madvise with flag MADV_DONTNEED (for thread destruction), and avoid new signal handler creation. */
    scmp_filter_ctx scmp;
    /* This is a blacklist sandbox which means that by default, unknown syscalls are allowed. */
    scmp = seccomp_init(SCMP_ACT_ALLOW);
    seccomp_add_rules(SANDBOX_KILL_SANDBOX, SANDBOX_MORE_RIGHTS, scmp);
    /* Load seccomp filter */
    return seccomp_load(scmp);
}

/* Set sandbox level on current thread */
int vlc_set_sandbox_level_spec(uint64_t flags, int level, char **name) {
	/* Init seccmp filter for current thread */
	scmp_filter_ctx scmp;
	/* Whitelist sandbox */
	scmp = seccomp_init(SCMP_ACT_TRAP);
	/* Add rules according to sandbox flags and level */
	seccomp_add_rules(flags, level, scmp);
	/* Load seccomp filter */
	return seccomp_load(scmp);
}

/* Apply a new signal handler and immediatly set a seccomp filter according to arguments. */
/* TODO: This function is used by handle_allowed_clone which is still EXPERIMENTAL */
int vlc_duplicate_broker_spec(uint64_t flags, int level, char **name) {
    /* Set signal handler */
	if(init_sigaction() != 0) {
        printf("VLC Sandbox (PID: %ld): Sigaction wasn't set properly ...\n", get_current_pid());
        return 1;
    }

    /* Set seccomp filter */
    scmp_filter_ctx scmp;
    scmp = seccomp_init(SCMP_ACT_TRAP);
    seccomp_add_rules(flags, level, scmp);
    /* Load seccomp filter */
    return seccomp_load(scmp);
}

/* TID of the current thread on Linux */
long get_current_tid() {
	return syscall(SYS_gettid);
}

/* PID of the current thread on Linux */
long get_current_pid() {
	return syscall(SYS_getpid);
}

/* Init os sandbox node data */
vlc_sandbox_node_data_t *init_sandbox_node_data_spec() {
    vlc_sandbox_node_data_t *vlc_sandbox_node_data = malloc (sizeof (struct vlc_sandbox_node_data_t));
    if (vlc_sandbox_node_data == NULL)
        return NULL;
    vlc_sandbox_node_data->stack_node_head = NULL;
    return vlc_sandbox_node_data;
}

/* Delete sandbox node data */
int delete_sandbox_node_data_spec(vlc_sandbox_node_data_t *vlc_sandbox_node_data) {
    vlc_stack_node_t *node = vlc_sandbox_node_data->stack_node_head;
    while(node != NULL) {
        vlc_stack_node_t *temp = node;
        node = node->next;
        /* printf("VLC Sandbox (PID: %ld): deleting stack %p\n", get_current_pid(), temp->stack_starting_addr); */
        if (temp != NULL) {
            free(temp);
        }
    }
    return 0;
}

/* Init sandbox_request os specific struct */
int init_sandbox_request_data_spec(vlc_sandbox_request_t *sandbox_request) {
    sandbox_request->vlc_sandbox_request_data = malloc(sizeof(struct vlc_sandbox_node_data_t));
    return 0;
}

/* Delete sandbox_request os specific struct */
int delete_sandbox_request_data_spec(vlc_sandbox_request_t *sandbox_request) {
    free(sandbox_request->vlc_sandbox_request_data);
    return 0;
}

/* Print OS sandbox node data */
void print_vlc_sandbox_node_data_spec(vlc_sandbox_node_data_t *node) {
	vlc_stack_node_t *cursor;
	cursor = node->stack_node_head;
	printf("VLC Sandbox (PID: %ld): ------------------------\n", get_current_pid());
	printf("VLC Sandbox (PID: %ld): |MMAP stack linked list|\n", get_current_pid());
	printf("VLC Sandbox (PID: %ld): ------------------------\n", get_current_pid());
	while(cursor != NULL) {
		printf("VLC Sandbox (PID: %ld): MMAP stack first addr : %p\n", get_current_pid(), cursor->stack_starting_addr);
		printf("VLC Sandbox (PID: %ld): MMAP stack size : %lu\n", get_current_pid(), cursor->stack_size);
		printf("VLC Sandbox (PID: %ld): MMAP stack last addr : %p\n", get_current_pid(), ((intptr_t) cursor->stack_starting_addr + (intptr_t) cursor->stack_size));
		printf("VLC Sandbox (PID: %ld): TID: %d\n", get_current_pid(), cursor->associated_tid);
		printf("VLC Sandbox (PID: %ld): ------------------------\n", get_current_pid());
		cursor = cursor->next;
	}
}
