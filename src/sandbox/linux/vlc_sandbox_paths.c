/***********************************************************************************
* linux/vlc_sandbox_paths.c: Linux paths for each flag in vlc_sandbox_capabilities.h
************************************************************************************/

#include "vlc_sandbox_paths.h"
#include <limits.h>
#include <stdlib.h>

/**********************************************************************************
 * Static prototypes
 **********************************************************************************/
/* Resolve path to an absolute existing path */
static int get_resolved_path(const char *path, char *resolved_path);
/* Get paths allowed by capability flag */
static const char** get_paths(uint64_t flag);
/* Get nb of paths allowed by capability flag */
static int get_nb_of_paths(uint64_t flag);
/* Path comparison function */
static int paths_comparison(char *resolved_path, char *allowed_path);

/**********************************************************************************
 * Paths definitions
 **********************************************************************************/
/*
	WARNING: when adding new paths, keep in mind that
	 	- paths have to be in alphabetical order
		- update the nb of paths for each flag
*/

/* ############################ BLACK PATHS ############################ */
static const char sandbox_blacklisted_paths[][PATH_MAX + 1] = {
	"/home/Guillaume/.ssh"
};
static int nb_of_blacklisted_paths = 1;

/* ############################ WHITE PATHS ############################ */
/* Read access to these paths is whitelisted */
static const char sandbox_whitelisted_paths[][PATH_MAX + 1] = {
	"/lib",
	"/media/Guillaume/DATA/Documents/VLC/vlc-repo/build/",
	"/proc",
	"/usr/lib"
};
static int nb_of_whitelisted_paths = 4;

/* ############################ GREY PATHS ############################ */
/* SANDBOX_AUDIO */
static const char sandbox_audio_paths[][PATH_MAX + 1] = {
	NULL
};
static int nb_of_audio_paths = 0;

/* SANDBOX_VIDEO */
static const char sandbox_video_paths[][PATH_MAX + 1] = {
	"/dev/dri",
	"/run/user/1000/gdm/Xauthority",
	"/sys/bus/pci",
	"/sys/devices"
};
static int nb_of_video_paths = 4;

/* SANDBOX_FONTS */
static const char sandbox_fonts_paths[][PATH_MAX + 1] = {
	"/etc/fonts",
	"/usr",
	"/usr/share/fontconfig",
	"/usr/share/fonts",
	"/usr/share/poppler",
	"/usr/share/texmf/fonts",
	"/var/cache/fontconfig"
};
static int nb_of_fonts_paths = 7;

/* SANDBOX_CONFIG */
static const char sandbox_config_paths[][PATH_MAX + 1] = {
	"/etc"
};
static int nb_of_config_paths = 1;

/* SANDBOX_READ_USER_FILE & SANDBOX_WRITE_USER_FILE */
static const char sandbox_user_file_paths[][PATH_MAX + 1] = {
	"/home/Guillaume",
	"/media",
	"/sys/devices/system"
};
static int nb_of_user_file_paths = 3;

/* SANDBOX_READ_DEV */
static const char sandbox_dev_paths[][PATH_MAX + 1] = {
	NULL
};
static int nb_of_dev_paths = 0;

/* SANDBOX_READ_CDDVD */
static const char sandbox_CD_DVD_paths[][PATH_MAX + 1] = {
	NULL
};
static int nb_of_CD_DVD_paths = 0;

static const char** get_paths(uint64_t flag) {
	switch (flag) {
		case SANDBOX_AUDIO:
			return sandbox_audio_paths;
		case SANDBOX_VIDEO:
			return sandbox_video_paths;
		case SANDBOX_FONTS:
			return sandbox_fonts_paths;
		case SANDBOX_CONFIG:
			return sandbox_config_paths;
		case SANDBOX_READ_USER_FILE:
		case SANDBOX_WRITE_USER_FILE:
			return sandbox_user_file_paths;
		case SANDBOX_READ_DEV:
			return sandbox_dev_paths;
		case SANDBOX_READ_CDDVD:
			return sandbox_CD_DVD_paths;
		default:
			return NULL;
	}
}

static int get_nb_of_paths(uint64_t flag) {
	switch (flag) {
		case SANDBOX_AUDIO:
			return nb_of_audio_paths;
		case SANDBOX_VIDEO:
			return nb_of_video_paths;
		case SANDBOX_FONTS:
			return nb_of_fonts_paths;
		case SANDBOX_CONFIG:
			return nb_of_config_paths;
		case SANDBOX_READ_USER_FILE:
		case SANDBOX_WRITE_USER_FILE:
			return nb_of_user_file_paths;
		case SANDBOX_READ_DEV:
			return nb_of_dev_paths;
		case SANDBOX_READ_CDDVD:
			return nb_of_CD_DVD_paths;
		default:
			return 0;
	}
}

/**********************************************************************************
 * Access checking functions
 **********************************************************************************/
/* Check path read/write access permission according to security policies in node */
int check_path_access(const char *path, vlc_syscall_t *vlc_syscall, vlc_sandbox_node_t *node, uint64_t request_type) {

	printf("VLC Broker (PID: %ld): (%s, %ld, syscall: %ld) Checking path '%s'\n", get_current_pid(), node->name, node->tid, vlc_syscall->syscall, path);

	char *resolved_path = malloc(PATH_MAX);
	if (get_resolved_path(path, resolved_path)) {
		printf("VLC Broker (PID: %ld): Resolved path is '%s'\n", get_current_pid(), resolved_path);
	} else {
		printf("VLC Broker (PID %ld): Path '%s' couldn't be resolved ...\n", get_current_pid(), path);
		return 0;
	}

	/* Check if resolved_path is blacklisted */
	if (bsearch( resolved_path, sandbox_blacklisted_paths, (size_t) (nb_of_blacklisted_paths), PATH_MAX + 1, (void *)paths_comparison ) != NULL) {
		/* Path is blacklisted! Deny access */
		printf("VLC Broker (PID: %ld): This path is blacklisted!\n", get_current_pid(), resolved_path);
		return 0;
	}

	/* Check if resolved_path is whitelisted */
	if (bsearch( resolved_path, sandbox_whitelisted_paths, (size_t) (nb_of_whitelisted_paths), PATH_MAX + 1, (void *)paths_comparison ) != NULL) {
		/* Path is whitelisted! Give access if read request */
		if (HAS_FLAG(request_type, SANDBOX_READ_REQUEST)) {
			printf("VLC Broker (PID: %ld): This path is whitelisted!\n", get_current_pid(), resolved_path);
			return 1;
		}
	}

	/* For each flag, check if absolute_path matches one of the listed paths */
	uint64_t flag_temp = 1;
	for (int i=0; i < SANDBOX_FLAG_MAX; i++) {
		flag_temp = 1 << i;
		if (HAS_FLAG(node->flags, flag_temp)) {
			/* Check path */
			if (bsearch(resolved_path, get_paths(flag_temp), (size_t) (get_nb_of_paths(flag_temp)), PATH_MAX + 1, (void *)paths_comparison) != NULL) {
				/* Path matched! Check access privileges */
				if (HAS_FLAG(request_type, SANDBOX_WRITE_REQUEST)) {
					if (HAS_FLAG(SANDBOX_WRITE_FLAGS, flag_temp)) {
						printf("VLC Broker (PID: %ld): Path allowed with read & write privileges !\n", get_current_pid());
						return 1;
					} else {
						printf("VLC Broker (PID: %ld): Path not writable!\n", get_current_pid());
					}
				} else {
					printf("VLC Broker (PID: %ld): Path allowed with read privilege !\n", get_current_pid());
					return 1;
				}
			}
		}
	}
	/* Path not found, deny access */
	printf("VLC Broker (PID: %ld): Path %s not recognized, acces denied!\n", get_current_pid(), path);
	return 0;
}

static int get_resolved_path(const char *path, char *resolved_path) {
	/* Check if file exists or reduce file to the last existing path */
	size_t path_length = strlen(path);
	char *path_temp = malloc(PATH_MAX);
	path_temp = strncpy(path_temp, path, path_length);
	if (access(path_temp, F_OK) != 0) {
		/* File doesn't exists, remove one character after the other from the end, until the path exists */
		path_length--;
		path_temp = strncpy(path_temp, path_temp, path_length);
		path_temp[path_length] = '\0';
		while((access(path_temp, F_OK) == -1) && (path_length > 0)) {
			path_length--;
			path_temp = strncpy(path_temp, path_temp, path_length);
			path_temp[path_length] = '\0';
		}
		if (access(path_temp, F_OK) == -1) {
			return 0;
		}
	}
	/* Clean requested path */
	resolved_path = realpath(path_temp, resolved_path);
	if (resolved_path == NULL) {
		/* Path couldn't be resolved - deny access */
		return 0;
	} else {
		return 1;
	}
}

/* Path comparison function */
/* For optimisation purposes, allowed_paths should be ordered alphabetically */
static int paths_comparison(char *resolved_path, char *allowed_path) {
	if ((resolved_path != NULL) && (allowed_path != NULL)) {
		long resolved_path_len = strlen(resolved_path);
		long allowed_path_len = strlen(allowed_path);

		if ((resolved_path_len > 0) && (allowed_path_len > 0)) {
			/* Copy the minimum number of element to compare into buffers */
			if (allowed_path_len > resolved_path_len) {
				/* Nothing to compare, resolved_path too short */
				return -1;
			} else {
				/* Check if the first allowed_path_len element of resolved_path matches allowed_path */
				return strncmp(resolved_path, allowed_path, allowed_path_len);
			}
		} else {
			return -1;
		}
	} else {
		return -1;
	}
}
