/***********************************************************************************
* linux/vlc_sandbox_paths.h: Linux paths for each flag in vlc_sandbox_capabilities.h
************************************************************************************/

#include "../vlc_sandbox_internal.h"

/* Check path read/write access permission according to security policies in node */
int check_path_access(const char *path, vlc_syscall_t *vlc_syscall, vlc_sandbox_node_t *node, uint64_t request_type);
