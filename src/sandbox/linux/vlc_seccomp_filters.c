/********************************************************
 * vlc_seccomp_filter.c: Main filters used to sandbox VLC
 ********************************************************/

#include <signal.h>
#include <seccomp.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "vlc_seccomp_filters.h"
#include "../vlc_sandbox_internal.h"

/* ############################ LIST ############################ */
/* Each function adds a category of syscalls. For each category, you might find a WHITE LIST and / or a GREY LIST and / or a BLACK LIST. Ommitted syscalls are automatically blacklisted (technically trapped by kernel but rejected by default by broker). */

/* Seccomp rules required by the sandbox to work - (more precisely to track thread parenting and avoid new signal handlers) */
static void seccomp_add_sandbox_required_rules(scmp_filter_ctx scmp);
/* Some syscalls are safe and can be added to every sandboxed thread */
static void seccomp_add_safe_rules(scmp_filter_ctx scmp);
/* Add pthread_create syscalls to seccomp filter */
static void seccomp_deny_admin_syscall(scmp_filter_ctx scmp);

/* Add syscalls to allow a child thread to add its own more restrictive filter */
static void seccomp_add_son_filter_rules(scmp_filter_ctx scmp);
/* Deny permission to create son filters */
static void seccomp_deny_son_filter(scmp_filter_ctx scmp);

/* Trap syscalls with a path in its params */
static void seccomp_path_rules(scmp_filter_ctx scmp);
/* Add fd creation syscalls to seccomp filter */
static void seccomp_add_fd_creation_rules(scmp_filter_ctx scmp);
/* Add reading syscalls to seccomp filter */
static void seccomp_add_fd_read_rules(scmp_filter_ctx scmp);
/* Add writing syscalls to seccomp filter */
static void seccomp_add_fd_write_rules(scmp_filter_ctx scmp);


/* Seccomp rules required by the sandbox to work - (more precisely to track thread parenting and avoid new signal handlers) */
static void seccomp_add_sandbox_required_rules(scmp_filter_ctx scmp) {
    /* ############################ GREY LIST ############################ */
    /* Used to track thread parents */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(mmap), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(set_robust_list), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(madvise), 0);
    /* Used to destroy new signal handlers */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(rt_sigprocmask), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(rt_sigaction), 0);

    /* ############################ WHITE LIST ############################ */
    /* Used by sandbox handlers */
    /* basic memory management syscalls */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(fstatfs), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(futex), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 0);
    /* return from sigaction handler syscall */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigreturn), 0);
    /* socket IPC syscall */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(recvmsg), 0);
    /* exit syscalls */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
}

/* Some syscalls are considered as "safe" and won't be handled by the broker. Sandboxed will automatically be allowed to make those syscalls. Some syscalls of this list come from Chrome's sandbox:
    https://cs.chromium.org/chromium/src/sandbox/linux/seccomp-bpf-helpers/syscall_sets.cc  */
static void seccomp_add_safe_rules(scmp_filter_ctx scmp) {
    /* ############################ WHITE LIST ############################ */
    /* Time */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(nanosleep), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(clock_nanosleep), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(clock_gettime), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(gettimeofday), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(poll), 0);
    /* System / thread / process / file infos */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(sysinfo), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getegid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(geteuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getgid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getgroups), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getpid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getppid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getresgid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getresuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getsid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(gettid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getrusage), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(uname), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(capget), 0);
    /* give permission to right to stdout */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(write), 1, SCMP_A0(SCMP_CMP_EQ, STDOUT_FILENO));
    /* Memory management */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getrlimit), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(shmget), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(shmat), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(shmctl), 0);
    /* CPU management */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(sched_getaffinity), 0);
    /* IPC */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(wait4), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getsockname), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(pipe), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(pipe2), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(eventfd2), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(shutdown), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getpeername), 0);
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(tgkill), 0);
    /* Random number generator */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(getrandom), 0);
    /* File management */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(close), 0);
}

static void seccomp_new_thread_or_process(scmp_filter_ctx scmp) {
    /* WARNING - first and second args for clone are inverted comparred to what man says ... don't ask why! */

    /* ############################ WHITE LIST ############################ */
    /* Allow threads */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(clone), 1, SCMP_A0(SCMP_CMP_EQ,CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID));

    /* ############################ GREY LIST ############################ */
    /* But trap new processes */
    /* Trap fork syscall */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(clone), 1, SCMP_A0(SCMP_CMP_EQ, CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD));
    /* Trap vfork syscall */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(clone), 1, SCMP_A0(SCMP_CMP_EQ, CLONE_VM|CLONE_VFORK|SIGCHLD));

    /* ############################ BLACK LIST ############################ */
    /* Kill execve & fork -->> fork should be clones with the correct flags ... */
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(execve), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(fork), 0);
}

/* Deny access to any admin syscall */
static void seccomp_deny_admin_syscall(scmp_filter_ctx scmp) {
    /* ############################ GREY LIST ############################ */
    /* chmod should be killed immediatly, but VLC will crash without it ... */
    /* chmod is defined in seccomp_path_rules  */

    /* ############################ BLACK LIST ############################ */
    /* Those syscalls won't even go through the broker, they will immediatly get denied and the thread killed */
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(fallocate), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(fchmod), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(fchmodat), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(chown), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(lchown), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(fchown), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(fchownat), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setfsgid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setfsuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setgid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setgroups), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setregid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setresgid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setresuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setreuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setuid), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(kexec_load), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(reboot), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(setdomainname), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(sethostname), 0);
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(syslog), 0);
}

/* Add syscalls to allow a child thread to add its own more or less restrictive seccomp filter */
static void seccomp_add_son_filter_rules(scmp_filter_ctx scmp) {
    /* ############################ WHITE LIST ############################ */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(seccomp), 0);
  	seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(prctl), 0);
}

/* Deny permission to create son filters */
static void seccomp_deny_son_filter(scmp_filter_ctx scmp) {
    /* ############################ BLACK LIST ############################ */
    seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(seccomp), 0);
  	seccomp_rule_add(scmp, SCMP_ACT_KILL, SCMP_SYS(prctl), 0);
}

/* Add syscalls to allow a child thread to add its own more restrictive filter */
static void seccomp_path_rules(scmp_filter_ctx scmp) {
    /* ############################ BLACK LIST ############################ */
    /* chmod should be killed immediatly, but VLC will crash without it ... */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(chmod), 0);

    /* ############################ GREY LIST ############################ */
    /* Open, creat and memfd_create are defined in seccomp_add_fd_creation_rules */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(access), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(faccessat), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(futimesat), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(link), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(lstat), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(mkdir), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(mknod), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(mknodat), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(readlink), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(rename), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(rmdir), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(stat), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(symlink), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(unlink), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(uselib), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(utimes), 0);
	seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(truncate), 0);
}

/* Add fd creation syscalls to seccomp filter */
static void seccomp_add_fd_creation_rules(scmp_filter_ctx scmp) {
    /* ############################ WHITE LIST ############################ */
    /* TODO: no real path to check for this syscall ... but TRAP should be added in order to properly handle file descriptors later (memfd_create returns a fd) */
    seccomp_rule_add(scmp, SCMP_ACT_ALLOW, SCMP_SYS(memfd_create), 0);

    /* ############################ GREY LIST ############################ */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(open), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(socket), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(creat), 0);
}

/* Add reading syscalls to seccomp filter */
static void seccomp_add_fd_read_rules(scmp_filter_ctx scmp) {
    /* ############################ WHITE LIST ############################ */
    /* recvfrom, recvmsg are allowed by seccomp_add_sandbox_required_rules */

    /* ############################ GREY LIST ############################ */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(fadvise64), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(fstat), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(ioctl), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(read), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(readlinkat), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(readv), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(connect), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(getdents), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(lseek), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(fcntl), 0);
}

/* Add writing syscalls to seccomp filter */
static void seccomp_add_fd_write_rules(scmp_filter_ctx scmp) {
    /* ############################ GREY LIST ############################ */
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(write), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(writev), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(sendto), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(sendmsg), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(sendmmsg), 0);
    seccomp_rule_add(scmp, SCMP_ACT_TRAP, SCMP_SYS(ftruncate), 0);
}

void seccomp_add_rules(uint64_t flags, int level, scmp_filter_ctx scmp) {
    /* Add required syscalls to have a working sandbox */
    seccomp_add_sandbox_required_rules(scmp);
    /* Some syscalls are "safe" and won't be handled by the broker */
    seccomp_add_safe_rules(scmp);
    /* Some syscalls should never be called by VLC */
    seccomp_deny_admin_syscall(scmp);
    /* Allow threads and deny processes / execve */
    seccomp_new_thread_or_process(scmp);

    /* Allow or Deny son filters creation */
    if (HAS_FLAG(level, SANDBOX_LESS_RIGHTS) || HAS_FLAG(level, SANDBOX_MORE_RIGHTS)) {
        seccomp_add_son_filter_rules(scmp);
    } else {
        seccomp_deny_son_filter(scmp);
    }

    if (!HAS_FLAG(flags, SANDBOX_KILL_SANDBOX)) {
        /* Trap syscalls with a path in its params  */
        seccomp_path_rules(scmp);
        /* Add file descriptor creation filters */
        seccomp_add_fd_creation_rules(scmp);
        /* Add read syscalls filters */
        seccomp_add_fd_read_rules(scmp);
        /* Add writing syscalls filters */
        seccomp_add_fd_write_rules(scmp);
    }
}
