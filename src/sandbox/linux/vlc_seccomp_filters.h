/*****************************************************************************
 * vlc_seccomp_filter.h: Main filters used to sandbox for VLC
 *****************************************************************************/

/* Add rules accoring to flags and level of sandbox node */
void seccomp_add_rules(uint64_t flags, int level, scmp_filter_ctx scmp);
