/********************************************
 * vlc_syscall_sets.c: Define sets of syscall
 ********************************************/

#include "vlc_syscall_sets.h"

int is_sandbox_required_syscall(long sysno) {
	switch (sysno) {
		/* Used to track thread parents */
		case SYS_mmap:
		case SYS_set_robust_list:
		case SYS_madvise:
		/* Used to destroy new signal handlers */
		case SYS_rt_sigprocmask:
		case SYS_rt_sigaction:
		/* basic memory management syscalls */
		case SYS_futex:
		case SYS_mprotect:
		case SYS_munmap:
		/* return from sigaction handler syscall */
		case SYS_rt_sigreturn:
		/* socket IPC syscall */
		case SYS_recvfrom:
		case SYS_recvmsg:
		/* exit syscalls */
		case SYS_exit_group:
		case SYS_exit:
			return 1;
		default:
			return 0;
	}
};

int is_safe_syscall(long sysno) {
	switch (sysno) {
		/* time */
		case SYS_nanosleep:
	    case SYS_clock_nanosleep:
	    case SYS_clock_gettime:
	    case SYS_gettimeofday:
	    case SYS_poll:
	    /* System / thread / process / file infos */
	    case SYS_sysinfo:
	    case SYS_getegid:
	    case SYS_geteuid:
	    case SYS_getgid:
	    case SYS_getgroups:
	    case SYS_getpid:
	    case SYS_getppid:
	    case SYS_getresgid:
	    case SYS_getresuid:
	    case SYS_getsid:
	    case SYS_gettid:
	    case SYS_getuid:
		case SYS_getrusage:
	    case SYS_uname:
	    case SYS_capget:
	    /* Memory management */
	    case SYS_getrlimit:
	    case SYS_shmget:
	    case SYS_shmat:
	    case SYS_shmctl:
	    /* CPU management */
	    case SYS_sched_getaffinity:
	    /* IPC */
		case SYS_wait4:
	    case SYS_getsockname:
	    case SYS_pipe:
	    case SYS_pipe2:
	    case SYS_eventfd2:
	    case SYS_shutdown:
	    case SYS_getpeername:
	    case SYS_tgkill:
	    /* Random number generator */
	    case SYS_getrandom:
	    /* File management */
	    case SYS_close:
			return 1;
		default:
			return 0;
	}
};

int is_admin_syscall(long sysno) {
	switch (sysno) {
		case SYS_fallocate:
	    case SYS_chmod:
	    case SYS_fchmod:
	    case SYS_fchmodat:
	    case SYS_chown:
	    case SYS_lchown:
	    case SYS_fchown:
	    case SYS_fchownat:
	    case SYS_setfsgid:
	    case SYS_setfsuid:
	    case SYS_setgid:
	    case SYS_setgroups:
	    case SYS_setregid:
	    case SYS_setresgid:
	    case SYS_setresuid:
	    case SYS_setreuid:
	    case SYS_setuid:
	    case SYS_kexec_load:
	    case SYS_reboot:
	    case SYS_setdomainname:
	    case SYS_sethostname:
	    case SYS_syslog:
			return 1;
		default:
			return 0;
	}
}

int is_thread_or_process_creation_syscall(long sysno) {
	switch (sysno) {
		case SYS_clone:
		case SYS_fork:
		case SYS_execve:
			return 1;
		default:
			return 0;
	}
}

int is_son_filter_syscall(long sysno) {
	switch (sysno) {
		case SYS_seccomp:
		case SYS_prctl:
			return 1;
		default:
			return 0;
	}
};

int is_syscall_with_path(long sysno) {
	switch (sysno) {
		case SYS_chmod:
		case SYS_chown:
		case SYS_lchown:
		case SYS_open:
		case SYS_creat:
		case SYS_memfd_create:
		case SYS_access:
		case SYS_faccessat:
		case SYS_futimesat:
		case SYS_link:
		case SYS_lstat:
		case SYS_mkdir:
		case SYS_mknod:
		case SYS_mknodat:
		case SYS_readlink:
		case SYS_rename:
		case SYS_rmdir:
		case SYS_stat:
		case SYS_symlink:
		case SYS_unlink:
		case SYS_uselib:
		case SYS_utimes:
		case SYS_truncate:
			return 1;
		default:
			return 0;
	}
}

int is_fd_creation_syscall(long sysno) {
	switch (sysno) {
		case SYS_open:
		case SYS_socket:
		case SYS_memfd_create:
		case SYS_creat:
			return 1;
		default:
			return 0;
	}
};

int is_fd_read_syscall(long sysno) {
	switch (sysno) {
		case SYS_recvfrom:
		case SYS_recvmsg:
		case SYS_fadvise64:
		case SYS_fstat:
	    case SYS_ioctl:
	    case SYS_read:
	    case SYS_readlink:
	    case SYS_readlinkat:
	    case SYS_readv:
		case SYS_connect:
	    case SYS_getdents:
	    case SYS_lseek:
	    case SYS_fcntl:
			return 1;
		default:
			return 0;
	}
};

int is_fd_write_syscall(long sysno) {
	switch (sysno) {
	    case SYS_write:
	    case SYS_writev:
	    case SYS_sendto:
	    case SYS_sendmsg:
		case SYS_sendmmsg:
		case SYS_setsockopt:
		case SYS_ftruncate:
			return 1;
		default:
			return 0;
	}
};
