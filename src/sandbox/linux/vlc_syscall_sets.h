/*********************************************
 * vlc_syscall_sets.h: Define sets of syscall
 *********************************************/

 #include "../vlc_sandbox_internal.h"

int is_sandbox_required_syscall(long sysno);
int is_safe_syscall(long sysno);
int is_admin_syscall(long sysno);
int is_thread_or_process_creation_syscall(long sysno);
int is_add_son_filter_syscall(long sysno);
int is_fd_creation_syscall(long sysno);
int is_fd_read_syscall(long sysno);
int is_fd_write_syscall(long sysno);
int is_syscall_with_path(long sysno);
