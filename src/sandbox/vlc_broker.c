/*****************************************************************************
 * vlc_broker.h: VLC broker - allows or denies access to sandbox requests
 *****************************************************************************/

#include "vlc_sandbox_internal.h"

static int handle_shutdown_request();

/*****************************************************************************
 * Broker thread main function
 *****************************************************************************/
/* VLC threads will request access to some system ressources, the broker is here to grant or deny access to these ressources. */

void *broker_func(void *data) {

    libvlc_int_t *p_libvlc = data;
    int shutdown = 0;

    /* Release the syscall_request mutex to let sandboxed threads start making requests */
    /* This mutex is initialized at 0 instead of 1 to make sure that the broker is ready to go before the sandboxed threads. */
    vlc_sem_post(&sandbox_request->mutex);
    printf("VLC Broker (PID: %ld): Sandbox broker (TID: %ld) ready ...\n", get_current_pid(), get_current_tid());

    while(!shutdown) {
        /* Broker has to wait for a handler to wake him up */
        vlc_sem_wait(&sandbox_request->broker);

        /* An empty request is a shutdown request */
        if (sandbox_request->vlc_sandbox_request_data == NULL) {
            shutdown = handle_shutdown_request();
        } else {
            /* Filter out some calls (OS specific) */
            if (!filter_out_request(sandbox_request->vlc_sandbox_request_data)) {
                /* Find the security policies (vlc_sandbox_node_t) of the sandboxed thread */
                vlc_sandbox_node_t *node_temp = find_node_by_tid(sandbox_request->tid);
                if (node_temp != NULL) {
                    /* Check access according to the thread's security policies (OS specific) */
                    if (check_access(node_temp, sandbox_request->vlc_sandbox_request_data)) {
                        /* Access granted! Make call and write return code in sandbox_request */
                        handle_allowed_syscall(node_temp);
                    } else {
                        /* Access denied - handler will kill the thread */
                        sandbox_request->access = 0;
                        sandbox_request->return_code = -1;
                    }
                } else {
                    /* This thread has no policies yet. Being here means it was cloned by a sandboxed parent thread */
                    /* Try and find the parent of the thread (OS specific) */
                    vlc_sandbox_node_t *parent_node;
                    parent_node = find_parent_node(sandbox_request->vlc_sandbox_request_data);

                    if (parent_node != NULL) {
                        /* Parent found! Create security policies for the child according to the flags / level of the parent */
                        node_temp = add_sandbox_node(sandbox_request->tid, parent_node->flags, parent_node->level, parent_node->name, parent_node);
                        /* Check access according to the thread's security policies (OS specific) */
                        if (check_access(node_temp, sandbox_request->vlc_sandbox_request_data)) {
                            /* Access granted! Make call and write return code in sandbox_request */
                            handle_allowed_syscall(node_temp);
                        } else {
                            /* Access denied - handler will kill the thread */
                            sandbox_request->access = 0;
                            sandbox_request->return_code = -1;
                        }
                    } else {
                        /* This thread has no parent and no security policies ... kill it! */
                        printf("VLC Broker (PID: %ld): Parent not found ... \n", get_current_pid());
                        //print_sandbox_nodes();
                        /* Parent can't be found, thread can't be trusted, deny access */
                        sandbox_request->access = 0;
                        sandbox_request->return_code = -1;
                    }
                }
            }
        }
        /* unlock handler */
        vlc_sem_post(&sandbox_request->handler);
    }
    printf("VLC Broker (TID: %ld): Broker is down !\n", get_current_tid());
}

/**************************
 * Shutdown request handler
 **************************/
/* Check that the thread trying to shutdown the broker has the correct flag set. */
static int handle_shutdown_request() {
    printf("VLC Broker (TID: %ld): Shutdown request received!\n", get_current_tid());
    vlc_sandbox_node_t *node_temp = find_node_by_tid(sandbox_request->tid);
    if (node_temp != NULL) {
        if (HAS_FLAG(node_temp->flags, SANDBOX_KILL_SANDBOX)) {
            /* Shutdown broker */
            sandbox_request->return_code = 0;
            return 1;
        } else {
            /* Shutdown denied: the request sender doesn't have the correct flag. */
            sandbox_request->return_code = 1;
            return 0;
        }
    } else {
        /* Shutdown denied: the request sender doesn't have the correct flag. */
        sandbox_request->return_code = 1;
        return 0;
    }
}
