/*****************************************************************************
 * vlc_broker.h: VLC broker - allows or denies access to system ressources
 *****************************************************************************/

#include "vlc_sandbox_struct.h"

/* Broker thread main function */
void *broker_func(void *data);

/*****************************************************************************
* Parent thread <-> son thread matching functions
*****************************************************************************/
/* Find the parent thread of an unknown thread (OS specific) */
vlc_sandbox_node_t *find_parent_node(vlc_sandbox_request_data_t *vlc_sandbox_request_data);
/* Filter out some calls (OS specific) */
int filter_out_request(vlc_sandbox_request_data_t *vlc_sandbox_request_data);
/* Allow or deny access to vlc_syscall following node_temp security rule (OS specific) */
int check_access(vlc_sandbox_node_t *node_temp, vlc_sandbox_request_data_t *vlc_sandbox_request_data);
/* Handle allowed syscall (OS specific) */
void handle_allowed_syscall(vlc_sandbox_node_t *node);
