/*****************************************
 * vlc_sandbox.c: main functions & structs
 *****************************************/

#include "vlc_sandbox_internal.h"

/******************************
 * Global variable declarations
 ******************************/
vlc_sem_t sandbox_node_mutex;
vlc_sandbox_node_t *vlc_sandbox_node;
vlc_sandbox_request_t *sandbox_request;

/********************
 * Static prototypes
 ********************/
/* Init sandbox_request structure */
static struct vlc_sandbox_request_t *init_sandbox_request();
/* Recursive: Add or create sandbox_node_t */
static vlc_sandbox_node_t *add_sandbox_node_rec(vlc_sandbox_node_t *new_node, vlc_sandbox_node_t *node, long tid, uint64_t flags, int level, char **name, vlc_sandbox_node_t *parent);
/* Recursive: Print vlc_sandbox head */
static void print_sandbox_node_rec(vlc_sandbox_node_t *node);
/* Free sandbox node struct */
static int free_sandbox_node(vlc_sandbox_node_t *node);

/**********************************
 * Sandbox initialization functions
 **********************************/

/* Initialize sandbox_request structure */
static struct vlc_sandbox_request_t *init_sandbox_request() {
    // Try to allocate structure.
    vlc_sandbox_request_t *sandbox_request = malloc (sizeof (struct vlc_sandbox_request_t));
    if (sandbox_request == NULL)
        return NULL;

    /* Init sandbox_request */
    /* WARNING: sandbox_request mutex is initialized to 0 and will be released once by the broker thread as soon as the broker is ready. This is important to make sure that sandboxed threads wait for the broker before starting to make syscalls. */
    vlc_sem_init(&sandbox_request->mutex, 0);
    /* This sem is used to stop handlers until the broker answers */
    vlc_sem_init(&sandbox_request->handler, 0);
    /* Initial values */
    sandbox_request->access = 0;
    sandbox_request->tid = 0;

    /* OS specific initialization */
    if (init_sandbox_request_data_spec(sandbox_request) != 0) {
        printf("VLC Sandbox (TID: %ld): Error creating os sandbox_request data struct ...\n", get_current_tid());
        return NULL;
    }

    return sandbox_request;
}

/* Delete sandbox_request structure */
int delete_sandbox_request() {
    /* Free sandbox_request data (OS specific) */
    if (delete_sandbox_request_data_spec(sandbox_request) != 0) {
        printf("VLC Sandbox (TID: %ld): Error deleting sandbox_request os struct\n", get_current_tid());
        return 1;
    }
    free(sandbox_request);
    sandbox_request = NULL;
    return 0;
}

/* Init VLC sandbox */
int vlc_sandbox_init(libvlc_int_t *p_libvlc) {
    /* Sandbox request structure used to send data from handler to broker */
    sandbox_request = init_sandbox_request();
    /* Init mutex on sandbox_node struct and sandbox_node struct */
    vlc_sem_init(&sandbox_node_mutex, 1);

    /* Spawns broker thread */
    vlc_thread_t broker_thread;
    int return_code = vlc_clone(&broker_thread, broker_func, p_libvlc, VLC_THREAD_PRIORITY_HIGHEST);

    /* Add general rule for main thread */
    add_sandbox_node(get_current_tid(), SANDBOX_KILL_SANDBOX, SANDBOX_MORE_RIGHTS, "main", NULL);

    /* OS specific sandbox starting function */
    if(vlc_sandbox_init_spec(p_libvlc) != 0) {
        printf("VLC Sandbox (PID: %ld): OS specific sandbox starting function error ...\n", get_current_pid());
        return 1;
    }
    return return_code;
}

/* Shutdown VLC sandbox - only threads with flag SANDBOX_KILL_SANDBOX can do this */
int vlc_sandbox_shutdown() {
    /* Send a shutdown request (empty request) to the broker */
    vlc_sem_wait(&sandbox_request->mutex);
    sandbox_request->tid = get_current_tid();
    sandbox_request->vlc_sandbox_request_data = NULL;
    vlc_sem_post(&sandbox_request->broker);
    /* Wait for broker's response */
    vlc_sem_wait(&sandbox_request->handler);
    switch (sandbox_request->return_code) {
        case 0:
            printf("VLC Sandbox (TID: %ld): Broker stopped !\n", get_current_tid());
        case 1:
            printf("VLC Sandbox (TID: %ld): A thread without SANDBOX_KILL_SANDBOX tried to shutdown the broker !!\n", get_current_tid());
            return 1;
        case -1:
        default:
            printf("VLC Sandbox (TID: %ld): The broker didn't stop properly ...\n", get_current_tid());
            return 2;
    }
    /* Delete data structures */
    delete_sandbox_nodes();
    delete_sandbox_request();
    return 0;
}

/* Set sandbox level on current thread */
int vlc_set_sandbox_level(uint64_t flags, int level, char **name) {
    /* Update / set security policies in vlc_sandbox_node for the current thread */
    add_sandbox_node(get_current_tid(), flags, level, name, NULL);

    /* OS specific sandbox setting function */
    if(vlc_set_sandbox_level_spec(flags, level, name) != 0) {
        printf("VLC Sandbox (PID: %ld): OS specific sandbox setting function error ...\n", get_current_pid());
        return 1;
    }

    return 0;
}

/* Duplicate broker */
int vlc_duplicate_broker(uint64_t flags, int level, char **name) {

    /* Re-init the request structure used to send data from handler to broker */
    sandbox_request = init_sandbox_request();
    /* Re-init mutex on sandbox_node struct and sandbox_node struct */
    vlc_sem_init(&sandbox_node_mutex, 1);

    /* Spawns broker thread */
    vlc_thread_t broker_thread;
    int return_code = vlc_clone(&broker_thread, broker_func, NULL, VLC_THREAD_PRIORITY_HIGHEST);

    /* Set sandbox security policies (flags / level) for the new process */
    add_sandbox_node(get_current_tid(), flags, level, name, NULL);

    /* OS specific broker duplication function */
    if(vlc_duplicate_broker_spec(flags, level, name) != 0) {
        printf("VLC Sandbox (PID: %ld): OS specific sandbox starting function error ...\n", get_current_pid());
        return 1;
    }
    return return_code;
}

/*****************************
 * vlc_sandbox_node_t handling
 *****************************/
/* Get pointer to head of vlc_sandbox_node */
vlc_sandbox_node_t *get_sandbox_node_head_rec(vlc_sandbox_node_t *node) {
    if (node == NULL) {
        return NULL;
    } else {
        if (node->prev == NULL) {
            return node;
        } else {
            return get_sandbox_node_head_rec(node->prev);
        }
    }
}

/* Add / create or update the security policies of a thread */
vlc_sandbox_node_t *add_sandbox_node(long tid, uint64_t flags, int level, char **name, vlc_sandbox_node_t *parent_node) {
    printf("VLC Sandbox (PID: %ld): Adding rule %s (flags: %d, level: %d, tid: %ld) to vlc_sandbox_node ...\n", get_current_pid(), name, flags, level, tid);
    /* We need to malloc the new node BEFORE vlc_sem_wait sandbox_node_mutex otherwise there can be a deadlock when this function is called from a vlc_set_sandbox_level (and therefore from a sandboxed thread) */
    /* (malloc triggers a mmap call, which is caught by seccomp_filter, ...) */
    vlc_sandbox_node_t *new_node = malloc(sizeof(struct vlc_sandbox_node_t));

	vlc_sem_wait(&sandbox_node_mutex);
    /* If the vlc_sandbox_node is empty, then create a new rule */
    if (vlc_sandbox_node == NULL) {
        /* Init vlc_sandbox_node, the linked list of sandbox rules (flags / level) for each thread */
        vlc_sandbox_node = malloc(sizeof(struct vlc_sandbox_node_t));
        vlc_sandbox_node->level = level;
        vlc_sandbox_node->tid = tid;
        vlc_sandbox_node->flags = flags;
        vlc_sandbox_node->name = name;
        vlc_sandbox_node->writable = 1;
        vlc_sandbox_node->vlc_sandbox_node_data = init_sandbox_node_data_spec();
        vlc_sandbox_node->parent_tid = -1;
        vlc_sandbox_node->parent = NULL;
        vlc_sandbox_node->next = NULL;
        vlc_sandbox_node->prev = NULL;
        vlc_sem_post(&sandbox_node_mutex);
        free(new_node);
        return vlc_sandbox_node;
    } else {
        vlc_sandbox_node_t *node_temp = add_sandbox_node_rec(new_node, vlc_sandbox_node, tid, flags, level, name, parent_node);
        vlc_sem_post(&sandbox_node_mutex);
        return node_temp;
    }
}

/* Add / create or update the security policies of a thread - recursive auxiliary function  */
static vlc_sandbox_node_t *add_sandbox_node_rec(vlc_sandbox_node_t *new_node, vlc_sandbox_node_t *node, long tid, uint64_t flags, int level, char **name, vlc_sandbox_node_t *parent) {
    /* If the current thread already has security policies ... */
	if ((node->tid == tid)) {
        /* Parent might be NULL (when a sandboxed thread vlc_set_sandbox_level for instance) */
        if (parent == NULL) {
            /* Use the already existing parent */
            parent = node->parent;
        }
        /* Basic checks on "parent" argument to make sure that a thread doesn't try to falsify its parent. */
        if ((node->parent->tid == parent->tid) && (EQUAL_FLAG(node->parent->flags, parent->flags)) && (EQUAL_FLAG(node->parent->level, parent->level))) {
            /* Check if this node is writable */
            if (node->writable == 0) {
                printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (Not writable node)\n", get_current_pid(), tid);
                exit(1);
            } else {
                /* Check if parent allows new rules */
                if (HAS_FLAG(parent->level, SANDBOX_LESS_RIGHTS)) {
                    /* Check if son flags are included in parent flags */
                    if (HAS_FLAG(parent->flags, flags)) {
                        node->level = level;
                        node->tid = tid;
                        node->flags = flags;
                		node->name = name;
                        node->writable = 0;
                        free(new_node);
                        return node;
                    } else {
                        printf("VLC Broker (PID: %ld): Thread %ld is trying to set more flags than its parent (which has SANDBOX_LESS_RIGHTS flag) !!\n", get_current_pid(), tid);
                        exit(1);
                    }
                } else if (HAS_FLAG(parent->level, SANDBOX_MORE_RIGHTS)) {
                    /* nothing to check on son flags */
                    node->level = level;
                    node->tid = tid;
                    node->flags = flags;
            		node->name = name;
                    node->writable = 0;
                    free(new_node);
                    return node;
                } else if (HAS_FLAG(parent->level, SANDBOX_EQUAL_RIGHTS)) {
                    if (EQUAL_FLAG(parent->level, level) && EQUAL_FLAG(parent->flags, flags)) {
                        node->level = parent->level;
                        node->tid = tid;
                        node->flags = parent->flags;
                		node->name = name;
                        node->writable = 0;
                        free(new_node);
                        return node;
                    } else {
                        printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (parent with SANDBOX_EQUAL_RIGHTS)", get_current_pid(), tid);
                        exit(1);
                    }
                } else {
                    printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (parent with SANDBOX_DENY_ALL)", get_current_pid(), tid);
                    exit(1);
                }
            }
        } else {
            printf("VLC Broker (PID: %ld): Thread %ld is trying to falsify its parent !! (parent in sandbox_node and parent provided mismatch)\n", get_current_pid(), tid);
            exit(1);
        }
	} else if (tid < node->tid) {
		if (node->prev != NULL) {
			if (tid <= node->prev->tid) {
				return add_sandbox_node_rec(new_node, node->prev, tid, flags, level, name, parent);
			} else {
                if (parent == NULL) {
                    printf("VLC Broker (PID: %ld): Thread %ld is trying to set its own rules but has no parent !!\n", get_current_pid(), tid);
                    exit(1);
                }
                if ((HAS_FLAG(parent->level, SANDBOX_LESS_RIGHTS) && HAS_FLAG(parent->flags, flags)) || HAS_FLAG(parent->level, SANDBOX_MORE_RIGHTS) || (HAS_FLAG(parent->level, SANDBOX_EQUAL_RIGHTS) && EQUAL_FLAG(parent->level, level) && EQUAL_FLAG(parent->flags, flags))) {
    				new_node->level = level;
    				new_node->tid = tid;
                    new_node->parent_tid = parent->tid;
                    new_node->parent = parent;
                    new_node->writable = 1;
    				new_node->flags = flags;
    				new_node->name = name;
                    new_node->vlc_sandbox_node_data = init_sandbox_node_data_spec();
    				new_node->prev = node->prev;
    				new_node->next = node;
    				node->prev->next = new_node;
    				node->prev = new_node;
                    return new_node;
                } else {
                    printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (new node)", get_current_pid(), tid);
                    exit(1);
                }
			}
		} else {
            if (parent == NULL) {
                printf("VLC Broker (PID: %ld): Thread %ld is trying to set its own rules but has no parent !!\n", get_current_pid(), tid);
                exit(1);
            }
            if ((HAS_FLAG(parent->level, SANDBOX_LESS_RIGHTS) && HAS_FLAG(parent->flags, flags)) || HAS_FLAG(parent->level, SANDBOX_MORE_RIGHTS) || (HAS_FLAG(parent->level, SANDBOX_EQUAL_RIGHTS) && EQUAL_FLAG(parent->level, level) && EQUAL_FLAG(parent->flags, flags))) {
    			new_node->level = level;
    			new_node->tid = tid;
                new_node->parent_tid = parent->tid;
                new_node->parent = parent;
    			new_node->flags = flags;
    			new_node->name = name;
                new_node->writable = 1;
                new_node->vlc_sandbox_node_data = init_sandbox_node_data_spec();
    			new_node->prev = NULL;
    			new_node->next = node;
    			node->prev = new_node;
                return new_node;
            } else {
                printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (new node)", get_current_pid(), tid);
                exit(1);
            }
		}
	} else {
        if (node->next != NULL) {
            if (tid >= node->next->tid) {
                return add_sandbox_node_rec(new_node, node->next, tid, flags, level, name, parent);
            } else {
                if (parent == NULL) {
                    printf("VLC Broker (PID: %ld): Thread %ld is trying to set its own rules but has no parent !!\n", get_current_pid(), tid);
                    exit(1);
                }
                if ((HAS_FLAG(parent->level, SANDBOX_LESS_RIGHTS) && HAS_FLAG(parent->flags, flags)) || HAS_FLAG(parent->level, SANDBOX_MORE_RIGHTS) || (HAS_FLAG(parent->level, SANDBOX_EQUAL_RIGHTS) && EQUAL_FLAG(parent->level, level) && EQUAL_FLAG(parent->flags, flags))) {
    				new_node->level = level;
    				new_node->tid = tid;
                    new_node->parent_tid = parent->tid;
                    new_node->parent = parent;
    				new_node->flags = flags;
    				new_node->name = name;
                    new_node->writable = 1;
                    new_node->vlc_sandbox_node_data = init_sandbox_node_data_spec();
    				new_node->prev = node;
    				new_node->next = node->next;
    				node->next->prev = new_node;
    				node->next = new_node;
                    return new_node;
                } else {
                    printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (new node)", get_current_pid(), tid);
                    exit(1);
                }
            }
        } else {
            if (parent == NULL) {
                printf("VLC Broker (PID: %ld): Thread %ld is trying to set its own rules but has no parent !!\n", get_current_pid(), tid);
                exit(1);
            }
            if ((HAS_FLAG(parent->level, SANDBOX_LESS_RIGHTS) && HAS_FLAG(parent->flags, flags)) || HAS_FLAG(parent->level, SANDBOX_MORE_RIGHTS) || (HAS_FLAG(parent->level, SANDBOX_EQUAL_RIGHTS) && EQUAL_FLAG(parent->level, level) && (parent->flags, flags))) {
    			new_node->level = level;
    			new_node->tid = tid;
                new_node->parent_tid = parent->tid;
                new_node->parent = parent;
    			new_node->flags = flags;
    			new_node->name = name;
                new_node->writable = 1;
                new_node->vlc_sandbox_node_data = init_sandbox_node_data_spec();
    			new_node->prev = node;
    			new_node->next = NULL;
    			node->next = new_node;
                return new_node;
            } else {
                printf("VLC Broker (PID: %ld): Thread %ld is trying to overwrite its security policies !! (new node)", get_current_pid(), tid);
                exit(1);
            }
        }
	}
}

/* Find sandbox_node by tid */
vlc_sandbox_node_t *find_node_by_tid(long tid) {
    vlc_sem_wait(&sandbox_node_mutex);
    if (vlc_sandbox_node->tid == 0) {
        vlc_sem_post(&sandbox_node_mutex);
        return NULL;
    } else {
    	vlc_sandbox_node_t *node_temp = find_node_by_tid_rec(vlc_sandbox_node, tid);
    	vlc_sem_post(&sandbox_node_mutex);
    	return node_temp;
    }
}

/* Find sandbox_node by tid - recursive auxiliary function */
vlc_sandbox_node_t *find_node_by_tid_rec(vlc_sandbox_node_t *node, long tid) {
    if (tid == -1) {
        return NULL;
    }
	if (tid == node->tid) {
		return node;
	} else if (tid < node->tid) {
		if ((node->prev != NULL) && (tid <= node->prev->tid)) {
			return find_node_by_tid_rec(node->prev, tid);
		} else {
			return NULL;
		}
	} else {
		if ((node->next != NULL) && (tid >= node->next->tid)) {
			return find_node_by_tid_rec(node->next, tid);
		} else {
			return NULL;
		}
	}
}

/* Delete all nodes in vlc_sandbox_node linked list */
int delete_sandbox_nodes() {
    vlc_sandbox_node_t *node = get_sandbox_node_head_rec(vlc_sandbox_node);
    while(node != NULL) {
        vlc_sandbox_node_t *temp = node;
        node = node->next;
        /* printf("VLC Sandbox (PID: %ld): deleting node %ld\n", get_current_pid(), temp->tid); */
        if (free_sandbox_node(temp) != 0) {
            printf("VLC Sandbox (TID: %ld): Error deleting vlc_sandbox_node_data for sandbox_node %d\n", get_current_tid(), temp->tid);
            return 1;
        }
    }
    vlc_sandbox_node = NULL;
    return 0;
}

/* Delete a sandbox node in linked list */
int delete_sandbox_node(vlc_sandbox_node_t *node_temp) {
    if (vlc_sandbox_node == node_temp) {
        /* to avoid loosing reference to the linked list, set node to the next or previous node if not null */
        if (vlc_sandbox_node->next != NULL) {
            vlc_sandbox_node = vlc_sandbox_node->next;
        } else if (vlc_sandbox_node->prev != NULL) {
            vlc_sandbox_node = vlc_sandbox_node->prev;
        } else {
            /* Only one node in vlc_sandbox_node linked list, reset it to 0 and return */
            vlc_sandbox_node->tid = 0;
            return 0;
        }
    }
	if (node_temp != NULL) {
		/* delete node from linked list */
        if (node_temp->prev != NULL) {
            node_temp->prev->next = node_temp->next;
        }
        if (node_temp->next != NULL) {
            node_temp->next->prev = node_temp->prev;
        }
        return free_sandbox_node(node_temp);
	} else {
		return 1;
	}
}

/* Free node struct */
static int free_sandbox_node(vlc_sandbox_node_t *node) {
    /* Delete sandbox node data (OS specific) */
    if(delete_sandbox_node_data_spec(node->vlc_sandbox_node_data) != 0) {
        printf("VLC Sandbox (TID: %ld): Error deleting vlc_sandbox_node_data for sandbox_node %d\n", get_current_tid(), node->tid);
        return 1;
    }
    free(node);
    return 0;
}

/***********
 * Printing
 ***********/

 /* Print vlc_sandbox_node */
void print_sandbox_nodes() {
    vlc_sem_wait(&sandbox_node_mutex);
    if (vlc_sandbox_node != NULL) {
        vlc_sandbox_node_t *head = malloc(sizeof(struct vlc_sandbox_node_t));
        head = get_sandbox_node_head_rec(vlc_sandbox_node);
        if (head->tid == 0) {
            printf("VLC Sandbox (PID: %ld): Sandbox linked list is empty!\n", get_current_pid());
        } else {
            print_sandbox_node_rec(head);
        }
        vlc_sem_post(&sandbox_node_mutex);
    } else {
        printf("VLC Sandox (PID: %ld): Sandbox linked list is NULL!\n", get_current_tid());
    }
}

/* Print vlc_sandbox_node rec */
static void print_sandbox_node_rec(vlc_sandbox_node_t *node) {
    if (node == NULL) {
        printf("VLC Sandbox (PID: %ld): End of sandbox nodes.\n", get_current_pid());
    } else {
        printf("VLC Sandbox (PID: %ld): ------------------\n", get_current_pid());
        printf("VLC Sandbox (PID: %ld): |New sandbox node|\n", get_current_pid());
        printf("VLC Sandbox (PID: %ld): ------------------\n", get_current_pid());
        printf("VLC Sandbox (PID: %ld): Node thread : %d\n", get_current_pid(), node->tid);
        printf("VLC Sandbox (PID: %ld): Parent thread : %d\n", get_current_pid(), node->parent_tid);
        printf("VLC Sandbox (PID: %ld): Node name : %s\n", get_current_pid(), node->name);
        printf("VLC Sandbox (PID: %ld): Node flags : %p\n", get_current_pid(), node->flags);
        printf("VLC Sandbox (PID: %ld): Node level : %d\n", get_current_pid(), node->level);
        printf("VLC Sandbox (PID: %ld): Writable : %d\n", get_current_pid(), node->writable);
        print_vlc_sandbox_node_data_spec(node->vlc_sandbox_node_data);
        print_sandbox_node_rec(node->next);
    }
}
