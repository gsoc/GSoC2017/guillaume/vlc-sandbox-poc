/*****************************************************************************
* vlc_sandbox.h: Sandbox initialization functions
*****************************************************************************/
#ifndef VLC_SANDBOX_CAPABILITIES_H
#define VLC_SANDBOX_CAPABILITIES_H
#include "vlc_sandbox_capabilities.h"
#endif /* VLC_SANDBOX_CAPABILITIES_H */

/* Sandbox init function */
int vlc_sandbox_init(libvlc_int_t *p_libvlc);
/* Sandbox flags / level setting function */
int vlc_set_sandbox_level(uint64_t flags, int level, char **name);
/* Shutdown sandbox - only threads with flag SANDBOX_KILL_SANDBOX can do this */
int vlc_sandbox_shutdown();
