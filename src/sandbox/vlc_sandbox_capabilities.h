/****************************************************
 * vlc_sandbox_capabilities.h: Sanbox flags and level
 ****************************************************/

/* Macros to help handling flags */
#define HAS_FLAG(flag_set, flag) ((flag_set & flag) == flag)

#define EQUAL_FLAG(flag1, flag2) (HAS_FLAG(flag1, flag2) && HAS_FLAG(flag2, flag1))

/* Sandbox levels */
enum {
	SANDBOX_DENY_ALL = 1 << 0,
	SANDBOX_LESS_RIGHTS = 1 << 1,
	SANDBOX_EQUAL_RIGHTS = 1 << 2,
	SANDBOX_MORE_RIGHTS = 1 << 3
};

/* Sandbox flags */
typedef enum sandbox_flag {
	SANDBOX_NONE = 0,
	SANDBOX_KILL_SANDBOX = 1 << 0,
	SANDBOX_AUDIO = 1 << 1,
	SANDBOX_VIDEO = 1 << 2,
	SANDBOX_INTERNET_SOCKET = 1 << 3,
	SANDBOX_INTERNET_BIND = 1 << 4,
	SANDBOX_ALLOW_PROCESS = 1 << 5,
	SANDBOX_FONTS = 1 << 6,
	SANDBOX_CONFIG = 1 << 7,
	SANDBOX_WRITE_USER_FILE = 1 << 8,
	SANDBOX_READ_USER_FILE = 1 << 9,
	SANDBOX_READ_DEV = 1 << 10,
	SANDBOX_READ_CDDVD = 1 << 11,
} sandbox_flag;
#define SANDBOX_FLAG_MAX 12

/* List here all flags that allow writing in their allowed paths */
enum {
	SANDBOX_WRITE_FLAGS = SANDBOX_WRITE_USER_FILE | SANDBOX_VIDEO,
};

/* Sandbox request type - used to check paths */
enum {
	SANDBOX_READ_REQUEST = 1 << 0,
	SANDBOX_WRITE_REQUEST = 1 << 1,
};

/* Sandbox presets */
enum {
	/* Timer */
	SANDBOX_TIMER = SANDBOX_NONE,
	/* Playlist */
	SANDBOX_PLAYLIST = SANDBOX_NONE,
	SANDBOX_PLAYLIST_FETCHER = SANDBOX_READ_USER_FILE,
	/* Input */
	SANDBOX_INPUT = SANDBOX_READ_USER_FILE | SANDBOX_WRITE_USER_FILE | SANDBOX_FONTS | SANDBOX_CONFIG,
	/* Decoder */
	SANDBOX_DECODER = SANDBOX_CONFIG,
	/* Video output */
	SANDBOX_VIDEO_OUTPUT = SANDBOX_CONFIG | SANDBOX_READ_USER_FILE | SANDBOX_VIDEO | SANDBOX_FONTS
};
