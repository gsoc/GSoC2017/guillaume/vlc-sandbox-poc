/*****************************************************************************
* vlc_sandbox_internal.h: main internal functions of the sandbox
*****************************************************************************/

#ifndef VLC_SANDBOX_H
#define VLC_SANDBOX_H

#if defined (_WIN32)
#elif defined (__OS2__)
#elif defined (__ANDROID__)
#elif defined (__APPLE__)

#else /* Linux */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <seccomp.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <signal.h>
#include <linux/sched.h>
#include <ucontext.h>
#include <sys/wait.h>

#include "linux/vlc_handler.h"
#include "linux/vlc_broker.h"
#include "linux/vlc_seccomp_filters.h"

#endif /* LINUX */

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_interface.h>
#include <vlc_tls.h>

#include "vlc_sandbox.h"
#include "vlc_sandbox_struct.h"
#include "vlc_broker.h"

#ifndef VLC_SANDBOX_CAPABILITIES_H
#define VLC_SANDBOX_CAPABILITIES_H
#include "vlc_sandbox_capabilities.h"
#endif /* VLC_SANDBOX_CAPABILITIES_H */

/*****************************************************************************
* Sandbox initialization functions
*****************************************************************************/
/* Sandbox init function (OS specific) */
int vlc_sandbox_init_spec(libvlc_int_t *p_libvlc);
/* Sandbox flags / level setting function (OS specific) */
int vlc_set_sandbox_level_spec(uint64_t flags, int level, char **name);

/* Duplicate broker and set sandbox flags / level for the new process */
int vlc_duplicate_broker(uint64_t flags, int level, char **name);
/* Broker duplication function (OS specific) */
int vlc_duplicate_broker_spec(uint64_t flags, int level, char **name);

/* Init sandbox_request os struct */
int init_sandbox_request_data_spec(vlc_sandbox_request_t *sandbox_request);
/* Delete sandbox_request structure */
int delete_sandbox_request();
/* Delete sandbox_request (OS specific) */
int delete_sandbox_request_data_spec(vlc_sandbox_request_t *sandbox_request);

/* Get the tid of the current thread (OS specific) */
long get_current_tid();
/* Get the pid of the current thread (OS specific) */
long get_current_pid();

/*****************************************************************************
* vlc_sandbox_node_t handling
*****************************************************************************/
/* Init sandbox node data structure (OS specific) */
vlc_sandbox_node_data_t *init_sandbox_node_data_spec();
/* Delete all sandbox_node linked list */
int delete_sandbox_nodes();
/* Delete sandbox node in linked list */
int delete_sandbox_node(vlc_sandbox_node_t *node_temp);
/* Delete sandbox node data (OS specific) */
int delete_sandbox_node_data_spec(vlc_sandbox_node_data_t *vlc_sandbox_node_data);

/* Add or create sandbox_node_t */
vlc_sandbox_node_t *add_sandbox_node(long tid, uint64_t flags, int level, char **name, vlc_sandbox_node_t *parent);
/* Find sandbox_node */
vlc_sandbox_node_t *find_node_by_tid(long tid);
/* Recursive: Find sandbox_node */
vlc_sandbox_node_t *find_node_by_tid_rec(vlc_sandbox_node_t *node, long tid);
/* Rewing sandbox linked list and return head */
vlc_sandbox_node_t *get_sandbox_node_head_rec(vlc_sandbox_node_t *node);


/*****************************************************************************
* Printing
*****************************************************************************/
/* Print vlc sandbox nodes */
void print_sandbox_nodes();
/* Print OS sandbox node data */
void print_vlc_sandbox_node_data_spec(vlc_sandbox_node_data_t *node);

#endif /* VLC_SANDBOX_H */
