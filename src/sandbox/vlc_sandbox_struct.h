/*****************************************************************************
* vlc_sandbox_struc.h: main structures
*****************************************************************************/

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_interface.h>
#include <vlc_tls.h>

#ifndef VLC_SANDBOX_STRUCT_H
#define VLC_SANDBOX_STRUCT_H

/*****************************************************************************
* Struct definitions
*****************************************************************************/

/* This opac struct is used to store OS specific data about each vlc_sandbox_node */
typedef struct vlc_sandbox_node_data_t vlc_sandbox_node_data_t;
struct vlc_sandbox_node_data_t;

/* This opac struc is used to store OS specific data in the sandbox_request data structure */
typedef struct vlc_sandbox_request_data_t vlc_sandbox_request_data_t;
struct vlc_sandbox_request_data_t;

#if defined (_WIN32)
#elif defined (__OS2__)
#elif defined (__ANDROID__)
#elif defined (__APPLE__)

#else /* Linux */

typedef struct vlc_syscall_t vlc_syscall_t;
struct vlc_syscall_t {
    /* Syscall parameters */
    long syscall;
    intptr_t param1;
    intptr_t param2;
    intptr_t param3;
    intptr_t param4;
    intptr_t param5;
    intptr_t param6;

    /* info about the sandboxed thread */
    long tid;
    long uid;
};

/* This is used when looking for the parent of an orphan thread */
/* When a parent wants to clone a new thread, he starts by requesting memory to the kernel for the future stack of its child. More precisely, he does a mmap syscall with MAP_STACK flag, and a stack_size. This structure saves the return code of this syscall (the stack starting address) and the size of the stack. When the child has been identified, associated_tid will be set to the child's tid. */
/* A parent might have more than 1 child, which is the reason why we need to use a linked list */
typedef struct vlc_stack_node_t vlc_stack_node_t;
struct vlc_stack_node_t {
	vlc_stack_node_t *next;
	void *stack_starting_addr;
	size_t stack_size;
	int associated_tid;
};

struct vlc_sandbox_node_data_t {
    /* OS specific data on Linux of sandbox nodes */
    vlc_stack_node_t *stack_node_head;
};

struct vlc_sandbox_request_data_t {
    /* On Linux, the sandbox_request contains only the syscall parameters */
    vlc_syscall_t *vlc_syscall;
    /* TODO: (WORK IN PROGRESS) In order to allow the creation of processes within sandboxed threads, it might be interesting to dig a little bit more in the context jumping mechanism. This context is set by the handler. */
    ucontext_t *sandboxed_ctx;
};

/* After a madvise MADV_DONTNEED, stack addresses aren't really deallocated -> glibc saves them in cache to re-use them faster if a need thread is created by the process. Unfortunately this behavior breaks our parenting system. Because rt_sigprocmask are always called juste before a clone, we have to fall back to a really bad solution: hoping for the best. */
/* Save every call to rt_sigprocmask in a FIFO linked list. At the next set_robust_list call on a reallocated stack addr (in which case -> associated_tid = -2), his most probable parent will be the first element in list. */
typedef struct vlc_sigprocmask_node_t vlc_sigprocmask_node_t;
struct vlc_sigprocmask_node_t {
    long parent_tid;
    vlc_sigprocmask_node_t *next;
    vlc_sigprocmask_node_t *prev;
};

/*****************************************************************************
* Global variable declarations
*****************************************************************************/
extern vlc_sigprocmask_node_t *sandbox_sigprocmask_head_node;

#endif /* LINUX */

/* This struct is used to send data from a handler to the broker */
typedef struct vlc_sandbox_request_t vlc_sandbox_request_t;
struct vlc_sandbox_request_t {
    /* Synchronization */
    /* Protects this structure */
    vlc_sem_t mutex;
    /* Synchronization between multiple handlers / 1 broker */
    vlc_sem_t broker;
    /* Synchronization between multiple handlers / 1 broker */
    vlc_sem_t handler;

    /* TID of the thread making the request */
    long tid;

    /* Request specific data */
    vlc_sandbox_request_data_t *vlc_sandbox_request_data;

    /* 0 if access was denied - 1 if access was granted */
    int access;
    /* Return code of the requested syscall - set to -1 if error or failed */
    intptr_t return_code;
};

/* Double linked chained to save security policies (flags/level) for each TID */
typedef struct vlc_sandbox_node_t vlc_sandbox_node_t;
struct vlc_sandbox_node_t {
    vlc_sandbox_node_t *prev;
    vlc_sandbox_node_t *next;

    /* TID of the thread on with the current rule is applied */
    long tid;
    /* TID of the parent - set to -1 if none */
    long parent_tid;

    /* Parent node */
    vlc_sandbox_node_t *parent;

    /* See vlc_sandbox_level.h for explanation */
    int level;
    uint64_t flags;
    /* For debugging / printing purposes */
    char **name;

    /* writable indicates if the current rule can be overwritten or not */
    int writable;

    /* This opac struct is used to store OS specific data about each vlc_sandbox_node */
    vlc_sandbox_node_data_t *vlc_sandbox_node_data;
};

/*****************************************************************************
* Global variable declarations
*****************************************************************************/
/* Global variable to communicate between broker / handler */
/* Defined in vlc_sandbox.c */
/* Protection of the sandbox_node structure */
extern vlc_sem_t sandbox_node_mutex;
extern vlc_sandbox_node_t *vlc_sandbox_node;
extern vlc_sandbox_request_t *sandbox_request;
#define PRINT_SANDBOX_REQUESTS 1

#endif /* VLC_SANDBOX_STRUCT_H */
